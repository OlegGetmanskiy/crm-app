Система постановки задач, позволяет контролировать работу фрилансеров и людей на удаленной работе. Программа сообщает о новых задачах и близких дедлайнах, а также служит файловым хранилищем и системой рассылок и личных сообщений.

![alt tag](http://i.imgur.com/oHIsTQT.png)

![alt tag](http://i.imgur.com/Uzi7jbp.png)

![alt tag](http://i.imgur.com/w0HEC4H.png)

![alt tag](http://i.imgur.com/fnuwfAm.png)