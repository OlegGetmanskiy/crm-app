/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.helper;

import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.TableRowSorter;
import org.getmansky.AppFrame;
import org.getmansky.Console;
import org.getmansky.SettingsFrame;
import org.getmansky.gui.MainMenuPane;
import tray.SystemTrayAdapter;
import tray.SystemTrayProvider;
import tray.TrayIconAdapter;

/**
 *
 * @author OlegusGetman
 */
public class Helper {
   
   public static final String currentBuild = "build 0.0.4.1 alpha";
   public static final InformativeDateFormat dateFormat
      = new InformativeDateFormat(new SimpleDateFormat("dd.MM.YYYY"));
   public static final InformativeDateTimeFormat datetimeFormat
      = new InformativeDateTimeFormat(new SimpleDateFormat("dd.MM.YYYY HH:mm"));
   public static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
   public static String savedLogin = "";
   public static String savedPassword = "";
   public static TrayIconAdapter trayIcon;
   public static Console console = new Console();
   public static Image windowIcon = createImage("org/getmansky/gui/graphics/window.png", "window icon");
    
   static {
      console.setVisible(false);
   }

   public static void showMessage(String message) {
      JOptionPane.showMessageDialog(null, message);
   }

   public static String utf8(String s) {
      String out = null;
      try {
         out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
      } catch (java.io.UnsupportedEncodingException e) {
         return null;
      }
      return out;
   }
   
   public static void fillComboBox(JComboBox combo, Object[] objects) throws IOException {
      for(Object obj : objects) {
         combo.addItem(obj);
      }
   }
   
   public static BufferedImage decodeImage(byte[] photo) {
      if(photo == null) return null;
      String photoStr = new String(photo);
      byte[] photoData = org.apache.commons.codec.binary.Base64.decodeBase64(photoStr);
      try {
         return ImageIO.read(new ByteArrayInputStream(photoData));
      } catch (IOException | NullPointerException ex) {
         return null;
      }
   }
   
   public static boolean ask(String question) {
      int dialogResult = JOptionPane.showConfirmDialog(null, question, "Вопрос", JOptionPane.YES_NO_OPTION);
      return dialogResult == JOptionPane.YES_OPTION;
   }
   
   public static String readableMBytes(long size) {
      return decimalFormat.format((float)((float)size / (1024*1024)));
   }
   
   public static Image createImage(String path, String description) {
      //URL imageURL = Helper.class.getResource(path);
      URL imageURL = ClassLoader.getSystemClassLoader().getResource(path);

      if (imageURL == null) {
         System.err.println("Resource not found: " + path);
         return null;
      } else {
         return (new ImageIcon(imageURL, description)).getImage();
      }
   }
   
   public static PopupMenu getTrayMenu() {
      PopupMenu menu = new PopupMenu();
      
      MenuItem miProfile = new MenuItem("Мой профиль");
      miProfile.addActionListener((ActionEvent e)->{
         AppFrame.instance.setVisible(true);
         AppFrame.instance.toFront();
         AppFrame.instance.repaint();
         AppFrame.instance.requestFocus();
         MainMenuPane.showSelfProfile();
      });
      
      MenuItem miSettings = new MenuItem("Настройки");
      miSettings.addActionListener((ActionEvent e) -> {
         new SettingsFrame();
      });
      
      MenuItem miUsers = new MenuItem("Мои коллеги");
      miUsers.addActionListener((ActionEvent e)->{
         AppFrame.instance.setVisible(true);
         AppFrame.instance.toFront();
         AppFrame.instance.repaint();
         AppFrame.instance.requestFocus();
         MainMenuPane.showUsers();
      });
      
      MenuItem miTasks = Tray.miTasks = new MenuItem("Мои задачи");
      miTasks.addActionListener((ActionEvent e)->{
         AppFrame.instance.setVisible(true);
         AppFrame.instance.toFront();
         AppFrame.instance.repaint();
         AppFrame.instance.requestFocus();
         MainMenuPane.showTasks();
      });
      
      MenuItem miMessages = Tray.miMessages = new MenuItem("Сообщения");
      miMessages.addActionListener((ActionEvent e)->{
         AppFrame.instance.setVisible(true);
         AppFrame.instance.toFront();
         AppFrame.instance.repaint();
         AppFrame.instance.requestFocus();
         MainMenuPane.showMessages();
      });
      
      MenuItem miExit = new MenuItem("Выход");
      miExit.addActionListener((ActionEvent e) -> {
         System.exit(0);
      });
      
      menu.add(miProfile);
      menu.add(miSettings);
      menu.addSeparator();
      menu.add(miUsers);
      menu.add(miTasks);
      menu.add(miMessages);
      menu.addSeparator();
      menu.add(miExit);
      
      return menu;
   }
   
   public static void setupTray() {
      SystemTrayProvider trayProv = new SystemTrayProvider();
      SystemTrayAdapter trayAdapter = trayProv.getSystemTray();
      URL imageURL = ClassLoader.getSystemClassLoader().getResource("org/getmansky/gui/graphics/tray/tray.gif");
      String tooltip = "Light Objective";

      trayIcon = trayAdapter.createAndAddTrayIcon(imageURL, tooltip, getTrayMenu());
      trayIcon.addActionListener((ActionEvent e) -> {
         AppFrame.instance.setVisible(true);
         AppFrame.instance.toFront();
         AppFrame.instance.repaint();
         AppFrame.instance.requestFocus();
      });
   }
   
   public static synchronized void playNotificationSound() {
      playSound("org/getmansky/sound/alert.wav");
   }
   
   private static synchronized void playSound(final String url) {
      new Thread(new Runnable() {
         public void run() {
            try {
               Clip clip = AudioSystem.getClip();
               AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                  new BufferedInputStream(ClassLoader.getSystemClassLoader().getResourceAsStream(url)));
               clip.open(inputStream);
               clip.start();
            } catch (Exception e) {
               e.printStackTrace();
            }
         }
      }).start();
   }
   
   public static void showTableContextMenu(MouseEvent e, JTable table, JPopupMenu popup) {
      int r = table.rowAtPoint(e.getPoint());
      if (r >= 0 && r < table.getRowCount()) {
         table.setRowSelectionInterval(r, r);
      } else {
         table.clearSelection();
      }

      int rowindex = table.getSelectedRow();
      if (rowindex < 0) {
         return;
      }
      if (e.isPopupTrigger() && e.getComponent() instanceof JTable) {
         popup.show(e.getComponent(), e.getX(), e.getY());
      }
   }
   
   public static String escapeFromHtml(String text) {
      return text.replace('<', '|').replace('>', '|').replace("||", "|")
               .split("\\|")[3]
               .split(" ")[0];
   }
   
   public static void unsortTable(JTable table) {
      List<RowSorter.SortKey> sortKeys = new ArrayList<>();
      for(int i = 0; i < table.getModel().getColumnCount(); i++) {
         sortKeys.add(new RowSorter.SortKey(i, SortOrder.UNSORTED));
      }
      table.getRowSorter().setSortKeys(sortKeys);
      ((TableRowSorter)table.getRowSorter()).sort();
   }
}
