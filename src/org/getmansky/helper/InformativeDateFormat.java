/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author OlegusGetman
 */
public class InformativeDateFormat {
   private SimpleDateFormat sdf;
   
   public InformativeDateFormat(SimpleDateFormat sdf) {
      this.sdf = sdf;
      sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
   }   
   
   /*public String format(Date date) {
      Date today = new Date();
      int days = Days.daysBetween(new DateTime(date), new DateTime(today)).getDays();
      if (days == 0) {
         return sdf.format(date) + " (сегодня)";
      } else if (days == 1) {
         return sdf.format(date) + " (вчера)";
      } else if (days > 0) {
         return sdf.format(date) + " (" + days + " дней назад)";
      } else {
         return sdf.format(date) + " (через " + (-days) + " дней)";
      }
   }*/
   
   public String format(Date date) {
      Date today = new Date();
      DateTime todayDt = new DateTime(today);
      DateTime dateDt = new DateTime(date);
      todayDt = todayDt.withTime(0, 0, 0, 1);
      dateDt = dateDt.withTime(0, 0, 0, 1);
      
      int days = Days.daysBetween(dateDt, todayDt).getDays();
      if (days == 0) {
         return sdf.format(date) + " (сегодня)";
      } else if (days == 1) {
         return sdf.format(date) + " (вчера)";
      } else if (days > 0) {
         return sdf.format(date) + " (" + days + " дней назад)";
      } else {
         return sdf.format(date) + " (через " + (-days) + " дней)";
      }
   }
   
   public Date parse(String text) throws ParseException {
      String date = text.split(" ")[0];
      return sdf.parse(text);
   }
   
   public static int daysBetween(Date date1, Date date2) {
      DateTime date1Dt = new DateTime(date1);
      DateTime date2Dt = new DateTime(date2);
      date1Dt = date1Dt.withTime(0, 0, 0, 1);
      date2Dt = date2Dt.withTime(0, 0, 0, 1);
      
      return Days.daysBetween(date1Dt, date2Dt).getDays();
   }
}
