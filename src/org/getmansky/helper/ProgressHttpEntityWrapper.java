/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.helper;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.entity.HttpEntityWrapper;

/**
 *
 * @author OlegusGetman
 */
public class ProgressHttpEntityWrapper extends HttpEntityWrapper {

   private final ProgressCallback progressCallback;

   public static interface ProgressCallback {
      public void progress(float progress);
   }

   public ProgressHttpEntityWrapper(final HttpEntity entity, final ProgressCallback progressCallback) {
      super(entity);
      this.progressCallback = progressCallback;
   }

   @Override
   public void writeTo(final OutputStream out) throws IOException {
      this.wrappedEntity.writeTo(out instanceof ProgressFilterOutputStream ? out : new ProgressFilterOutputStream(out, this.progressCallback, getContentLength()));
   }

   static class ProgressFilterOutputStream extends FilterOutputStream {

      private final ProgressCallback progressCallback;
      private long transferred;
      private final long totalBytes;

      ProgressFilterOutputStream(final OutputStream out, final ProgressCallback progressCallback, final long totalBytes) {
         super(out);
         this.progressCallback = progressCallback;
         this.transferred = 0;
         this.totalBytes = totalBytes;
      }

      @Override
      public void write(final byte[] b, final int off, final int len) throws IOException {
         //super.write(byte b[], int off, int len) calls write(int b)
         out.write(b, off, len);
         this.transferred += len;
         this.progressCallback.progress(getCurrentProgress());
      }

      @Override
      public void write(final int b) throws IOException {
         out.write(b);
         this.transferred++;
         this.progressCallback.progress(getCurrentProgress());
      }

      private float getCurrentProgress() {
         return ((float) this.transferred / this.totalBytes) * 100;
      }
   }
}
