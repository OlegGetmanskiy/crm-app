/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.net;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.JProgressBar;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.getmansky.AppFrame;
import org.getmansky.NewEntriesListener;
import org.getmansky.Settings;
import org.getmansky.helper.Helper;
import static org.getmansky.helper.Helper.utf8;
import org.getmansky.gui.frame.account.SelfProfileInfo;
import org.getmansky.helper.ProgressHttpEntityWrapper;
import org.getmansky.model.Profile;
import org.getmansky.model.RegisterProfile;
import org.getmansky.net.json.CrmFile;
import org.getmansky.net.json.status.AddTaskStatus;
import org.getmansky.net.json.status.ChangeEmailStatus;
import org.getmansky.net.json.status.ChangePasswordStatus;
import org.getmansky.net.json.status.StatusResponse;
import org.getmansky.net.json.GroupList;
import org.getmansky.net.json.Message;
import org.getmansky.net.json.News;
import org.getmansky.net.json.status.JsonStatus;
import org.getmansky.net.json.status.LoginStatus;
import org.getmansky.net.json.status.RegistrationStatus;
import org.getmansky.net.json.SelfProfile;
import org.getmansky.net.json.Task;
import org.getmansky.net.json.TaskCategory;
import org.getmansky.net.json.TaskCategoryList;
import org.getmansky.net.json.TaskPrioritiesList;
import org.getmansky.net.json.TaskPriority;
import org.getmansky.net.json.TasksList;
import org.getmansky.net.json.User;
import org.getmansky.net.json.UsersList;

/**
 *
 * @author OlegusGetman
 */
public class Network {
   
   private static final HttpClient http = HttpClientBuilder.create().build();
   private static final ObjectMapper mapper = new ObjectMapper();
   private static final SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");

   private static GroupList groupList;
   private static TaskPrioritiesList taskPrioritiesList;
   private static TaskCategoryList taskCategoryList;
   public static final Map<Long, User> cachedUsers = new HashMap();

   private static volatile int requestCount = 0;
   public static List<NewEntriesListener> newEntriesListeners = new ArrayList();
   
   private static String postRequest(String path, List<NameValuePair> params) throws IOException {
      try {
         HttpPost post = new HttpPost(Settings.serverAddress + path);
         post.setHeader("Accept-Encoding", "UTF-8");
         if (params != null) {
            post.setEntity(new UrlEncodedFormEntity(params));
         }
         ResponseHandler<String> responseHandler = new BasicResponseHandler();
         requestCount++;
         if(AppFrame.progressRequest != null) {
            //System.out.println("Processing request: "+path);
            AppFrame.progressRequest.setString(path);
            AppFrame.progressRequest.setIndeterminate(requestCount>0);
         }
         String responseBody = http.execute(post, responseHandler);
         if(path.contains("web")) System.out.println(responseBody);
         return responseBody;
      } catch(Exception e) {
         System.out.println(e.getMessage());
         throw e;
      } finally {
         requestCount--;
         if(AppFrame.progressRequest != null) {
            AppFrame.progressRequest.setString("");
            AppFrame.progressRequest.setIndeterminate(requestCount>0);
         }
      }
   }
   
   public static void uploadFile(File file, JProgressBar pbar) throws IOException {
      HttpPost post = new HttpPost(Settings.serverAddress + "/file/upload");
      MultipartEntityBuilder builder = MultipartEntityBuilder.create();
      builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
      builder.addPart("file", new FileBody(file));
      builder.addPart("name", new StringBody(utf8(file.getName()), ContentType.APPLICATION_FORM_URLENCODED));
      post.setEntity(new ProgressHttpEntityWrapper(builder.build(), (float progress) -> {
         pbar.setValue((int) progress);
      }));
      ResponseHandler<String> responseHandler = new BasicResponseHandler();
      http.execute(post, responseHandler);
      pbar.setValue(0);
   }
   
   public static boolean downloadFile(long id, String dest, final JProgressBar pbar) throws IOException {
      HttpPost post = new HttpPost(Settings.serverAddress + "file/download");
      post.setHeader("Accept-Encoding", "UTF-8");
      
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      post.setEntity(new UrlEncodedFormEntity(params));
     
      HttpResponse res = http.execute(post);
      HttpEntity entity = res.getEntity();
      if(entity != null) {
         long bytesRead = 0;
         long bytesTotal = entity.getContentLength();
         
         pbar.setStringPainted(true);
         try (InputStream in = entity.getContent()) {
            BufferedOutputStream bos;
            try (BufferedInputStream bis = new BufferedInputStream(in)) {
               bos = new BufferedOutputStream(new FileOutputStream(new File(dest)));
               int inByte;
               while ((inByte = bis.read()) != -1) {
                  //bytesRead += inByte;
                  bytesRead++;
                  if(bytesRead % 1024 == 0) {
                     pbar.setValue((int) (((float) bytesRead / (float) bytesTotal) * 100));
                     pbar.setString(Helper.readableMBytes(bytesRead) + " MB / " + Helper.readableMBytes(bytesTotal) + " MB");
                  }
                  bos.write(inByte);
               }
            }
            bos.close();
            pbar.setValue(0);
         } catch (IOException | RuntimeException ex) {
            throw ex;
         }
      }
      
      return false;
   }
   
   public static JsonStatus recoverPassword(String login) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("login", login));
      return ((JsonStatus) mapper.readValue(postRequest("api/recoverPassword", params), JsonStatus.class));
   }
   
   public static JsonStatus acceptRecoverPassword(String login, String code, String newPassword) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("login", login));
      params.add(new BasicNameValuePair("code", code));
      params.add(new BasicNameValuePair("newPassword", utf8(newPassword)));
      return ((JsonStatus) mapper.readValue(postRequest("api/acceptRecoverPassword", params), JsonStatus.class));
   }
   
   public static CrmFile[] listFiles(long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return ((CrmFileList) mapper.readValue(postRequest("file/list", params), CrmFileList.class)).getFiles();
   }
   
   public static CrmFile[] listMyFiles(long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return ((CrmFileList) mapper.readValue(postRequest("file/my", params), CrmFileList.class)).getFiles();
   }
   
   public static CrmFile[] filesByName(String name, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      params.add(new BasicNameValuePair("name", utf8(name)));
      return ((CrmFileList) mapper.readValue(postRequest("file/byName", params), CrmFileList.class)).getFiles();
   }
   
   public static CrmFile fileInfo(long id) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      return (CrmFile) mapper.readValue(postRequest("file/info", params), CrmFile.class);
   }
   
   public static JsonStatus deleteFile(long id) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      String response = postRequest("file/delete", params);
      return (JsonStatus) mapper.readValue(response, JsonStatus.class);
   }

   public static LoginStatus login(String login, String pw) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("login", utf8(login)));
      params.add(new BasicNameValuePair("password", utf8(pw)));
      String response = postRequest("api/login", params);
      return (LoginStatus) mapper.readValue(response, LoginStatus.class);
   }

   public static GroupList groups() throws IOException {
      if (groupList == null) {
         groupList = (GroupList) mapper.readValue(postRequest("api/groups", null), GroupList.class);
      }
      return groupList;
   }
   
   public static TaskPrioritiesList taskPriorities() throws IOException {
      if (taskPrioritiesList == null) {
         taskPrioritiesList = (TaskPrioritiesList) mapper.readValue(postRequest("api/taskPriorities", null), TaskPrioritiesList.class);
      }
      return taskPrioritiesList;
   }
   
   public static JsonStatus addTaskCategory(TaskCategory tp) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("title", Helper.utf8(tp.getTitle())));
      return (JsonStatus) mapper.readValue(postRequest("api/addTaskCategory", params), JsonStatus.class);
   }
   
   public static JsonStatus editTaskCategory(TaskCategory tp) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(tp.getId())));
      params.add(new BasicNameValuePair("title", Helper.utf8(tp.getTitle())));
      return (JsonStatus) mapper.readValue(postRequest("api/editTaskCategory", params), JsonStatus.class);
   }
   
   public static JsonStatus deleteTaskCategory(TaskCategory tp) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(tp.getId())));
      params.add(new BasicNameValuePair("title", Helper.utf8(tp.getTitle())));
      return (JsonStatus) mapper.readValue(postRequest("api/deleteTaskCategory", params), JsonStatus.class);
   }
   
   public static TaskCategoryList myTaskCategories() throws IOException {
      return (TaskCategoryList) mapper.readValue(postRequest("api/myTaskCategories", null), TaskCategoryList.class);
   }
   
   public static TaskCategoryList userTaskCategories(User user) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("userId", String.valueOf(user.getId())));
      return (TaskCategoryList) mapper.readValue(postRequest("api/userTaskCategories", params), TaskCategoryList.class);
   }
   
   public static TaskCategoryList taskCategories(boolean forceUpdate) throws IOException {
      if (taskCategoryList == null || forceUpdate) {
         taskCategoryList = (TaskCategoryList) mapper.readValue(postRequest("api/taskCategories", null), TaskCategoryList.class);
      }
      return taskCategoryList;
   }

   public static Profile selfProfile() throws IOException {
      return Profile.fromSelfProfile((SelfProfile) mapper.readValue(postRequest("api/self", null), SelfProfile.class));
   }

   public static ChangePasswordStatus changePassword(String newPassword) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("password", utf8(newPassword)));
      return (ChangePasswordStatus) mapper.readValue(postRequest("api/changePassword", params), ChangePasswordStatus.class);
   }
   
   public static ChangeEmailStatus changeEmail(String newEmail) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("email", newEmail));
      return (ChangeEmailStatus) mapper.readValue(postRequest("api/changeEmail", params), ChangeEmailStatus.class);
   }
   
   public static JsonStatus changePhone(String phone) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("phone", phone));
      return (JsonStatus) mapper.readValue(postRequest("api/changePhone", params), JsonStatus.class);
   }

   public static RegistrationStatus register(RegisterProfile profile) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("name", utf8(profile.getName())));
      params.add(new BasicNameValuePair("fname", utf8(profile.getFname())));
      params.add(new BasicNameValuePair("oname", utf8(profile.getOname())));
      params.add(new BasicNameValuePair("position", utf8(profile.getPosition())));
      params.add(new BasicNameValuePair("phone", utf8(profile.getPhone())));
      params.add(new BasicNameValuePair("groupId", utf8(String.valueOf(profile.getGroup().getId()))));
      params.add(new BasicNameValuePair("login", utf8(profile.getLogin())));
      params.add(new BasicNameValuePair("email", utf8(profile.getEmail())));
      
      if(emailExists(profile.getEmail()).getStatus().equals("ok")) {
         if(Helper.ask("Аккаунт с этим email уже существует в системе. Все равно зарегистрировать?")) {
            return (RegistrationStatus) mapper.readValue(postRequest("api/register", params), RegistrationStatus.class);
         } else {
            return null;
         }
      } else 
         return (RegistrationStatus) mapper.readValue(postRequest("api/register", params), RegistrationStatus.class);
   } 
   
   public static JsonStatus emailExists(String email) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("email", utf8(email)));
      return (JsonStatus) mapper.readValue(postRequest("api/emailExists", params), JsonStatus.class);
   }
   
   public static List<String> positions() throws IOException {
      PositionList l = (PositionList) mapper.readValue(postRequest("api/distinctPositions", null), PositionList.class);
      LinkedList<String> pl = new LinkedList();
      for(Position pos : l.getPositions()) {
         pl.add(pos.getTitle());
      }
      return pl;
   }

   public static UsersList users() throws IOException {
      return (UsersList) mapper.readValue(postRequest("api/users", null), UsersList.class);
   }
   
   public static User[] inactiveUsers(long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return ((UsersList) mapper.readValue(postRequest("api/inactiveUsers", params), UsersList.class)).getUsers();
   }
   
   public static JsonStatus activateUser(long id) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      return (JsonStatus) mapper.readValue(postRequest("api/activateUser", params), JsonStatus.class);
   }
   
   public static User user(long id) throws IOException {
      User user;
      if((user = cachedUsers.get(id)) != null) {
         return user;
      } else {
         List<NameValuePair> params = new ArrayList<>();
         params.add(new BasicNameValuePair("userId", String.valueOf(id)));
         user = (User) mapper.readValue(postRequest("api/user", params), User.class);
         user.setId(id);
         cachedUsers.put(id, user);
         return user;
      }
   }

   public static StatusResponse editUser(User user) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("name", utf8(user.getName())));
      params.add(new BasicNameValuePair("fname", utf8(user.getFname())));
      params.add(new BasicNameValuePair("oname", utf8(user.getOname())));
      params.add(new BasicNameValuePair("position", utf8(user.getPosition())));
      params.add(new BasicNameValuePair("groupId", utf8(String.valueOf(user.getGroupId()))));
      return (StatusResponse) mapper.readValue(postRequest("api/editUser", params), StatusResponse.class);
   }

   public static StatusResponse deleteUser(User user) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", utf8(String.valueOf(user.getId()))));
      return (StatusResponse) mapper.readValue(postRequest("api/deleteUser", params), StatusResponse.class);
   }
   
   public static TasksList myTasks(TaskCategory category, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(SelfProfileInfo.id))));
      params.add(new BasicNameValuePair("categoryId", utf8(String.valueOf(category.getId()))));
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return (TasksList) mapper.readValue(postRequest("api/tasksByUserId", params), TasksList.class);
   }
   
   public static Task[] myLiveTasks(long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(SelfProfileInfo.id))));
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return ((TasksList) mapper.readValue(postRequest("api/liveTasksByUserId", params), TasksList.class)).getTasks();
   }
   
   public static TasksList tasksByUser(User user, TaskCategory category, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("categoryId", utf8(String.valueOf(category.getId()))));
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return (TasksList) mapper.readValue(postRequest("api/tasksByUserId", params), TasksList.class);
   }
   
   public static TasksList tasksByUser(User user, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return (TasksList) mapper.readValue(postRequest("api/allTasksByUserId", params), TasksList.class);
   }
   
   public static TasksList activeTasksByUser(User user, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(user.getId()))));
      return (TasksList) mapper.readValue(postRequest("api/liveTasksByUserId", params), TasksList.class);
   }
   
   public static TasksList tasksByAuthor(User user, TaskCategory category, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("authorId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("categoryId", utf8(String.valueOf(category.getId()))));
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return (TasksList) mapper.readValue(postRequest("api/tasksByAuthorId", params), TasksList.class);
   }

   public static TasksList tasksByAuthor(User user, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("authorId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return (TasksList) mapper.readValue(postRequest("api/allTasksByAuthorId", params), TasksList.class);
   }

   public static TasksList activeTasksByAuthor(User user, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("authorId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return (TasksList) mapper.readValue(postRequest("api/liveTasksByAuthorId", params), TasksList.class);
   }
   
   public static Task task(User user, Task task) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("taskId", utf8(String.valueOf(task.getId()))));
      return (Task) mapper.readValue(postRequest("api/viewTask", params), Task.class);
   }
   
   public static AddTaskStatus addTask(User user, Task task) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      
      StringBuilder attachedFiles = new StringBuilder();
      if(task.getAttachedFiles() != null) {
         for (Long fileId : task.getAttachedFiles()) {
            attachedFiles.append(fileId).append(',');
         }
         if (attachedFiles.length() > 0) {
            attachedFiles.deleteCharAt(attachedFiles.length() - 1);
         }
      }      

      params.add(new BasicNameValuePair("files", utf8(attachedFiles.toString())));
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("endDate", utf8(dateFormat.format(task.getEndDate()))));
      params.add(new BasicNameValuePair("title", utf8(task.getTitle())));
      params.add(new BasicNameValuePair("description", utf8(task.getDescription())));
      params.add(new BasicNameValuePair("priorityId", utf8(String.valueOf(task.getPriorityId()))));
      params.add(new BasicNameValuePair("categoryId", utf8(String.valueOf(task.getCategoryId()))));
      return (AddTaskStatus) mapper.readValue(postRequest("api/addTask", params), AddTaskStatus.class);
   }
   
   public static JsonStatus editTask(Task task) throws IOException {
      List<NameValuePair> params = new ArrayList<>();

      StringBuilder attachedFiles = new StringBuilder();
      for (Long fileId : task.getAttachedFiles()) {
         attachedFiles.append(fileId).append(',');
      }
      if (attachedFiles.length() > 0) {
         attachedFiles.deleteCharAt(attachedFiles.length() - 1);
      }

      params.add(new BasicNameValuePair("taskId", String.valueOf(task.getId())));
      params.add(new BasicNameValuePair("text", utf8(task.getDescription())));
      return (JsonStatus) mapper.readValue(postRequest("api/editTask", params), JsonStatus.class);
   }
   
   public static void postponeTask(long id, Date date) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      params.add(new BasicNameValuePair("date", utf8(dateFormat.format(date))));
      postRequest("api/postponeTask", params);
   }
   
   public static void priorityTask(long id, TaskPriority p) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      params.add(new BasicNameValuePair("id_priority", String.valueOf(p.getId())));
      postRequest("api/priorityTask", params);
   }
   
   public static JsonStatus editSelfPhoto(BufferedImage photo) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      
      byte[] data;
      try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
         ImageIO.write(photo,"jpg",bos);
         //byte[] data = ((DataBufferByte)photo.getData().getDataBuffer()).getData();
         data = bos.toByteArray();
      }
      String img = org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString(data);
      
      params.add(new BasicNameValuePair("photo", img));
      return (JsonStatus) mapper.readValue(postRequest("api/changePhoto", params), JsonStatus.class);
   }
   
   public static JsonStatus completeTask(Task task, User user, boolean complete) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeId", utf8(String.valueOf(user.getId()))));
      params.add(new BasicNameValuePair("taskId", utf8(String.valueOf(task.getId()))));
      params.add(new BasicNameValuePair("complete", utf8(String.valueOf(complete))));
      return (JsonStatus) mapper.readValue(postRequest("api/completeTask", params), JsonStatus.class);
   }
   
   public static Message[] archiveMessages(long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return ((MessageList) mapper.readValue(postRequest("api/messages/viewArchive", params), MessageList.class)).getMessages();
   }
   
   public static Message[] inboxMessages(long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return ((MessageList) mapper.readValue(postRequest("api/messages/inbox", params), MessageList.class)).getMessages();
   }
   
   public static Message[] outboxMessages(long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      return ((MessageList) mapper.readValue(postRequest("api/messages/outbox", params), MessageList.class)).getMessages();
   }
   
   public static Message[] dialogMessages(long employeeId, long offset) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("offset", String.valueOf(offset)));
      params.add(new BasicNameValuePair("employeeId", String.valueOf(employeeId)));
      return ((MessageList) mapper.readValue(postRequest("api/messages/dialog", params), MessageList.class)).getMessages();
   }
   
   public static JsonStatus sendMessage(User userTo, String title, String text, List<CrmFile> files) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("employeeTo", String.valueOf(userTo.getId())));
      params.add(new BasicNameValuePair("title", utf8(title)));
      params.add(new BasicNameValuePair("text", utf8(text)));
      
      StringBuilder attachedFiles = new StringBuilder();
      for(CrmFile file : files) {
         attachedFiles.append(file.getId()).append(',');
      }
      if(attachedFiles.length() > 0)
         attachedFiles.deleteCharAt(attachedFiles.length()-1);
      
      params.add(new BasicNameValuePair("files", utf8(attachedFiles.toString())));
      
      return (JsonStatus) mapper.readValue(postRequest("api/messages/send", params), JsonStatus.class);
   }
   
   public static JsonStatus sendBroadcastMessage(
         boolean addressAdmins,
         boolean addressManagers,
         boolean addressEmployees,
         String title, String text, 
         List<CrmFile> files,
         List<User> customUsers) 
      throws IOException {
      
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("addressAdmins", addressAdmins ? "true" : "false"));
      params.add(new BasicNameValuePair("addressManagers", addressManagers ? "true" : "false"));
      params.add(new BasicNameValuePair("addressEmployees", addressEmployees ? "true" : "false"));
      
      params.add(new BasicNameValuePair("title", utf8(title)));
      params.add(new BasicNameValuePair("text", utf8(text)));

      StringBuilder attachedFiles = new StringBuilder();
      for (CrmFile file : files) {
         attachedFiles.append(file.getId()).append(',');
      }
      if (attachedFiles.length() > 0) {
         attachedFiles.deleteCharAt(attachedFiles.length() - 1);
      }
      
      StringBuilder addUsers = new StringBuilder();
      for (User user : customUsers) {
         addUsers.append(user.getId()).append(',');
      }
      if (addUsers.length() > 0) {
         addUsers.deleteCharAt(addUsers.length() - 1);
      }

      params.add(new BasicNameValuePair("files", utf8(attachedFiles.toString())));
      params.add(new BasicNameValuePair("users", utf8(addUsers.toString())));

      return (JsonStatus) mapper.readValue(postRequest("api/messages/broadcast", params), JsonStatus.class);
   }
   
   public static Message message(long id) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      return (Message) mapper.readValue(postRequest("api/messages/read", params), Message.class);
   }
   
   public static Message archiveMessage(long id) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      return (Message) mapper.readValue(postRequest("api/messages/archive", params), Message.class);
   }
   
   public static Message unarchiveMessage(long id) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("id", String.valueOf(id)));
      return (Message) mapper.readValue(postRequest("api/messages/unarchive", params), Message.class);
   }
   
   public static void archiveAllMessages() throws IOException {
      postRequest("api/messages/archiveAll", null);
   }
   
   public static void archiveAllOutboxMessages() throws IOException {
      postRequest("api/messages/archiveAllOutbox", null);
   }
   
   public static String employeesReport() throws IOException {
      String response = postRequest("web/reports/employees", null);
      return response;
   }
   
   public static String tasksReport(User user, Date from, Date to) throws IOException {
      List<NameValuePair> params = new ArrayList<>();
      params.add(new BasicNameValuePair("userId", String.valueOf(user.getId())));
      params.add(new BasicNameValuePair("dateFrom", utf8(dateFormat.format(from))));
      params.add(new BasicNameValuePair("dateTo", utf8(dateFormat.format(to))));
      String response = postRequest("web/reports/tasks", params);
      return response;
   }
   
   public static News news() throws IOException {
      News news = (News) mapper.readValue(postRequest("api/newEntries", null), News.class);
      new Thread(()->{
         for(NewEntriesListener listener : newEntriesListeners) {
            listener.onNewEntries(news);
         }
      }).start();
      return news;
   }
   
   private static class MessageList {
      private Message[] messages;

      public Message[] getMessages() {
         return messages;
      }

      public void setMessages(Message[] messages) {
         this.messages = messages;
      }
   }
   
   private static class CrmFileList {
      private CrmFile[] files;

      public CrmFile[] getFiles() {
         return files;
      }

      public void setFiles(CrmFile[] files) {
         this.files = files;
      }
   }
   
   private static class Position {
      
      @JsonProperty("position")
      private String title;

      public String getTitle() {
         return title;
      }

      public void setTitle(String title) {
         this.title = title;
      }
   }
   
   private static class PositionList {
      
      private Position[] positions;

      public Position[] getPositions() {
         return positions;
      }

      public void setPositions(Position[] positions) {
         this.positions = positions;
      }
   }
}