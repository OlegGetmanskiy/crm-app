/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.net.json;

/**
 *
 * @author OlegusGetman
 */
public class TaskPrioritiesList {
   private TaskPriority[] priorities;

   public TaskPriority findByTitle(String title) {
        for(TaskPriority g : priorities) {
            if(g.getTitle().equals(title))
                return g;
        }
        return null;
    }
    
    public TaskPriority findById(int id) {
        for(TaskPriority g : priorities) {
            if(g.getId() == id)
                return g;
        }
        return null;
    }
   
   public TaskPriority[] getPriorities() {
      return priorities;
   }

   public void setPriorities(TaskPriority[] priorities) {
      this.priorities = priorities;
   }
   
}
