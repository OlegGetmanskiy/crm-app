/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.net.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Calendar;
import java.util.Date;
import org.getmansky.gui.frame.files.AttachedFilesContainer;
import org.getmansky.helper.InformativeDateFormat;
import org.joda.time.DateTime;
import org.joda.time.Days;


/**
 *
 * @author OlegusGetman
 */
public class Task implements AttachedFilesContainer {
   
   private Long id;
   @JsonProperty("author_id")
   private Long authorId;
   @JsonProperty("employee_id")
   private Long employeeId;
   @JsonProperty("date_start")
   private Date startDate;
   @JsonProperty("date_end")
   private Date endDate;
   @JsonProperty("date_complete")
   private Date completeDate;
   private String title;
   private String description;
   private boolean accepted;
   @JsonProperty("priority_id")
   private int priorityId;
   private boolean complete;
   @JsonProperty("cat_id")
   private int categoryId;
   private boolean read;
   @JsonProperty("attached_files")
   private Long[] attachedFiles;

   public boolean isDead() {
      return !isComplete() && endDate.before(Calendar.getInstance().getTime());
   }
   
   public int getLateDays() {
      return Days.daysBetween(
         new DateTime(endDate).withTimeAtStartOfDay(), 
         new DateTime(completeDate).withTimeAtStartOfDay()
      ).getDays();
   }
   
   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Long getEmployeeId() {
      return employeeId;
   }

   public void setEmployeeId(Long employeeId) {
      this.employeeId = employeeId;
   }

   public Date getStartDate() {
      return startDate;
   }

   public void setStartDate(Date startDate) {
      this.startDate = startDate;
   }

   public Date getEndDate() {
      return endDate;
   }

   public void setEndDate(Date endDate) {
      this.endDate = endDate;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String desctiption) {
      this.description = desctiption;
   }

   public boolean isAccepted() {
      return accepted;
   }

   public void setAccepted(boolean accepted) {
      this.accepted = accepted;
   }

   public int getPriorityId() {
      return priorityId;
   }

   public void setPriorityId(int priorityId) {
      this.priorityId = priorityId;
   }
   
   public String toString() {
      return getTitle();
   }

   public boolean isComplete() {
      return complete;
   }

   public void setComplete(boolean complete) {
      this.complete = complete;
   }

   public int getCategoryId() {
      return categoryId;
   }

   public void setCategoryId(int categoryId) {
      this.categoryId = categoryId;
   }

   public boolean isRead() {
      return read;
   }

   public void setRead(boolean read) {
      this.read = read;
   }

   public Long getAuthorId() {
      return authorId;
   }

   public void setAuthorId(Long authorId) {
      this.authorId = authorId;
   }

   public Date getCompleteDate() {
      return completeDate;
   }

   public void setCompleteDate(Date completeDate) {
      this.completeDate = completeDate;
   }

   @Override
   public Long[] getAttachedFiles() {
      return attachedFiles;
   }

   public void setAttachedFiles(Long[] attachedFiles) {
      this.attachedFiles = attachedFiles;
   }
}
