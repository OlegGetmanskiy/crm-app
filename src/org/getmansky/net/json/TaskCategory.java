/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.net.json;

import java.util.Objects;

/**
 *
 * @author OlegusGetman
 */
public class TaskCategory {
   private int id;
   private String title;
   private Long count = 0L;

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public long getCount() {
      return count;
   }

   public void setCount(long count) {
      this.count = count;
   }
   
   @Override
   public String toString() {
      if(count != null && count != 0)
         return title + " ("+String.valueOf(count)+")";
      else
         return title;
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 41 * hash + Objects.hashCode(this.title);
      return hash;
   }

   @Override
   public boolean equals(Object obj) {
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      final TaskCategory other = (TaskCategory) obj;
      if (!Objects.equals(this.title, other.title)) {
         return false;
      }
      return true;
   }
   
   
}
