/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.net.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import org.getmansky.gui.frame.files.AttachedFilesContainer;

/**
 *
 * @author OlegusGetman
 */
public class Message implements AttachedFilesContainer {
   private long id;
   @JsonProperty("id_empl_from")
   private long idEmployeeFrom;
   @JsonProperty("id_empl_to")
   private long idEmployeeTo;
   private Date datetime;
   private String text;
   private String title;
   private boolean read;
   @JsonProperty("attached_files")
   private Long[] attachedFiles;
   private boolean broadcast = false;
   @JsonProperty("archive_from")
   private boolean archiveFrom;
   @JsonProperty("archive_to")
   private boolean archiveTo;

   public long getId() {
      return id;
   }

   public void setId(long id) {
      this.id = id;
   }

   public long getIdEmployeeFrom() {
      return idEmployeeFrom;
   }

   public void setIdEmployeeFrom(long idEmployeeFrom) {
      this.idEmployeeFrom = idEmployeeFrom;
   }

   public long getIdEmployeeTo() {
      return idEmployeeTo;
   }

   public void setIdEmployeeTo(long idEmployeeTo) {
      this.idEmployeeTo = idEmployeeTo;
   }

   public Date getDatetime() {
      return datetime;
   }

   public void setDatetime(Date datetime) {
      this.datetime = datetime;
   }

   public String getText() {
      return text;
   }

   public void setText(String text) {
      this.text = text;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public boolean isRead() {
      return read;
   }

   public void setRead(boolean read) {
      this.read = read;
   }

   @Override
   public Long[] getAttachedFiles() {
      return attachedFiles;
   }

   public void setAttachedFiles(Long[] attachedFiles) {
      this.attachedFiles = attachedFiles;
   }

   public boolean isBroadcast() {
      return broadcast;
   }

   public void setBroadcast(boolean broadcast) {
      this.broadcast = broadcast;
   }

   public boolean isArchiveFrom() {
      return archiveFrom;
   }

   public void setArchiveFrom(boolean archiveFrom) {
      this.archiveFrom = archiveFrom;
   }

   public boolean isArchiveTo() {
      return archiveTo;
   }

   public void setArchiveTo(boolean archiveTo) {
      this.archiveTo = archiveTo;
   }
   
   
}
