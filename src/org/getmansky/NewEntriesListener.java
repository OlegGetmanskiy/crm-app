/*
 This file is part of Light Objective.

 Light Objective is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Light Objective is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

 Copyright Oleg Getmansky 2015
 */
package org.getmansky;

import org.getmansky.net.json.News;

/**
 *
 * @author OlegusGetman
 */
public interface NewEntriesListener {
   public void onNewEntries(News news);
}
