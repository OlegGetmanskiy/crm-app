/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.model;

import java.io.IOException;
import java.util.Date;
import org.getmansky.net.Network;
import org.getmansky.net.json.Task;
import org.getmansky.net.json.TaskCategory;
import org.getmansky.net.json.User;

/**
 *
 * @author OlegusGetman
 */
public class ModelledTask {
   private Long id;
   private User author;
   private User employee;
   private Date startDate;
   private Date endDate;
   private String title;
   private String description;
   private String category;
   private boolean accepted;
   private boolean complete;
   private String priority;
   private boolean read;
   private boolean dead;

   public static ModelledTask fromJsonTask(Task t) {
      ModelledTask m = new ModelledTask();
      m.setId(t.getId());
      m.setStartDate(t.getStartDate());
      m.setEndDate(t.getEndDate());
      m.setTitle(t.getTitle());
      m.setDescription(t.getDescription());
      m.setAccepted(t.isAccepted());
      m.setComplete(t.isComplete());
      m.setRead(t.isRead());
      m.setDead(t.isDead());
      try {
         m.setPriority(Network.taskPriorities().findById(t.getPriorityId()).getTitle());
         m.setAuthor(Network.user(t.getAuthorId()));
         m.setEmployee(Network.user(t.getEmployeeId()));
         for(TaskCategory tCat : Network.taskCategories(false).getTaskCategories()) {
            if(tCat.getId() == t.getCategoryId()) {
               m.setCategory(tCat.getTitle());
               break;
            }
         }
      } catch (IOException ex) {
         
      }
      return m;
   }
   
   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Date getStartDate() {
      return startDate;
   }

   public void setStartDate(Date startDate) {
      this.startDate = startDate;
   }

   public Date getEndDate() {
      return endDate;
   }

   public void setEndDate(Date endDate) {
      this.endDate = endDate;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public boolean isAccepted() {
      return accepted;
   }

   public void setAccepted(boolean accepted) {
      this.accepted = accepted;
   }

   public String getPriority() {
      return priority;
   }

   public void setPriority(String priority) {
      this.priority = priority;
   }

   public boolean isComplete() {
      return complete;
   }

   public void setComplete(boolean complete) {
      this.complete = complete;
   }

   public boolean isRead() {
      return read;
   }

   public void setRead(boolean read) {
      this.read = read;
   }

   public boolean isDead() {
      return dead;
   }

   public void setDead(boolean dead) {
      this.dead = dead;
   }

   public User getAuthor() {
      return author;
   }

   public void setAuthor(User author) {
      this.author = author;
   }

   public User getEmployee() {
      return employee;
   }

   public void setEmployee(User employee) {
      this.employee = employee;
   }

   public String getCategory() {
      return category;
   }

   public void setCategory(String category) {
      this.category = category;
   }
   
   
}