/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.model;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.getmansky.helper.Helper;
import org.getmansky.net.Network;
import org.getmansky.net.json.SelfProfile;
import org.getmansky.net.json.User;

/**
 *
 * @author OlegusGetman
 */
public class Profile {

   private String login;
   private String name;
   private String fname;
   private String oname;
   private String groupName;
   private String position;
   private BufferedImage photo;

   public static Profile fromUser(User u) {
      Profile p = new Profile();
      p.setLogin(u.getLogin());
      p.setName(u.getName());
      p.setFname(u.getFname());
      p.setOname(u.getOname());
      p.setPosition(u.getPosition());
      try {
         p.setGroupName(Network.groups().findById(u.getGroupId()).getTitle());
         p.setPhoto(Helper.decodeImage(u.getPhoto()));
      } catch (IOException | NullPointerException ex) {
         Logger.getLogger(Profile.class.getName()).log(Level.SEVERE, null, ex);
      }
      return p;
   }
   
   public static Profile fromSelfProfile(SelfProfile sp) {
      Profile p = new Profile();
      p.setLogin(sp.getUser().getLogin());
      p.setName(sp.getUser().getName());
      p.setFname(sp.getUser().getFname());
      p.setOname(sp.getUser().getOname());
      p.setPosition(sp.getUser().getPosition());
      try {
         if(sp.getUser().getPhoto() != null) {
            p.setPhoto(Helper.decodeImage(sp.getUser().getPhoto()));
         }
         p.setGroupName(Network.groups().findById(sp.getUser().getGroupId()).getTitle());
      } catch (IOException e) {
         p.setGroupName("[Ошибка запроса]");
         //p.setGroupName(e.toString());
      }
      return p;
   }

   public String getFname() {
      return fname;
   }

   public void setFname(String fname) {
      this.fname = fname;
   }

   public String getOname() {
      return oname;
   }

   public void setOname(String oname) {
      this.oname = oname;
   }

   public String getLogin() {
      return login;
   }

   public void setLogin(String login) {
      this.login = login;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getGroupName() {
      return groupName;
   }

   public void setGroupName(String groupName) {
      this.groupName = groupName;
   }

   public BufferedImage getPhoto() {
      return photo;
   }

   public void setPhoto(BufferedImage photo) {
      this.photo = photo;
   }

   public String getPosition() {
      return position;
   }

   public void setPosition(String position) {
      this.position = position;
   }
}
