/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.gui;

import java.awt.TrayIcon;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.getmansky.AppFrame;
import org.getmansky.Settings;
import org.getmansky.gui.frame.account.SelfProfileInfo;
import org.getmansky.helper.Helper;
import org.getmansky.helper.Tray;
import org.getmansky.model.Profile;
import org.getmansky.net.Network;
import org.getmansky.net.json.News;

/**
 *
 * @author OlegusGetman
 */
public class MainMenuPane extends javax.swing.JPanel {

   private static final List<JPanel> openPanels = new LinkedList();
   
   public static JPanel selfProfilePane = new SelfProfilePane();
   public static JPanel usersPane = new UsersPane();
   public static TasksPane tasksPane = new TasksPane();
   public static MessagesPane messagesPane = new MessagesPane();
   public static JPanel fileStoragePane = new FileStoragePane();
   public static JPanel adminPane = new AdminPane();
   
   private final String originalTasksText;
   private final String originalMessagesText;
   public static boolean seenNotifications = false;
      
   /**
    * Creates new form MainMenuPane
    */
   public MainMenuPane(Profile self) {
      initComponents();
      
      originalTasksText = buttonTasks.getText();
      originalMessagesText = buttonMessages.getText();
      
      buttonManagement.setVisible(SelfProfileInfo.isAdminOrManager());
      
      Timer t = new Timer();
      t.scheduleAtFixedRate(new TimerTask() {
         @Override
         public void run() {
            receiveNews();
         }
      }, 0, 5000);
   }
   
   public void receiveNews() {
      try {
         News news = Network.news();
         long newTasks = news.getNewTasksCount();
         long newMessages = news.getNewMessagesCount();
         String infoTasks = "";
         String infoMessages = "";
         if(newTasks > 0) {
            infoTasks = "Новых задач: " + newTasks+" ";
            Tray.miTasks.setLabel("Задачи (новых: "+newTasks+")");
            buttonTasks.setText(
               originalTasksText.substring(0, originalTasksText.length() - 6)
               + "<b><font color=green>Новые задачи: "
               + newTasks
               + "</font></b></html>");
         } else {
            Tray.miTasks.setLabel("Задачи");
            buttonTasks.setText(originalTasksText);
         }
         if(newMessages > 0) {
            infoMessages = "Новых сообщений: " + newMessages+" ";
            Tray.miMessages.setLabel("Сообщения (новых: "+newMessages+")");
            buttonMessages.setText(
               originalMessagesText.substring(0, originalMessagesText.length() - 6)
               + "<b><font color=green>Новые сообщения: "
               + newMessages
               + "</font></b></html>");
         } else {
            Tray.miMessages.setLabel("Сообщения");
            buttonMessages.setText(originalMessagesText);
         }

         int notify = Settings.notificationBehavior;
         if(notify != Settings.NOTIFY_NONE) {
            String info;
            if(!(info = infoTasks+infoMessages).isEmpty() && !seenNotifications) {
               Helper.trayIcon.displayMessage("Новые вхождения", info, TrayIcon.MessageType.INFO);
               seenNotifications = true;
               Timer t = new Timer();
               t.schedule(new TimerTask() {
                  @Override
                  public void run() {
                     seenNotifications = false;
                  }
               }, 60000);

               if (notify == Settings.NOTIFY_TRAY_SOUND) {
                  Helper.playNotificationSound();
               }
            }
         }
      } catch (IOException ex) {
         Logger.getLogger(MainMenuPane.class.getName()).log(Level.SEVERE, null, ex);
      }
   }
   
   private static void openIfAbsent(String name, JPanel pane) {
      if(!openPanels.contains(pane)) {
         AppFrame.tabbedPane.add(name, new JScrollPane(pane));
         openPanels.add(pane);
      }
      AppFrame.tabbedPane.setSelectedIndex(AppFrame.tabbedPane.indexOfTab(name));
   }
   
   public static void freePanes() {
      selfProfilePane = null;
      usersPane = null;
      tasksPane = null;
      messagesPane = null;
      fileStoragePane = null;
      adminPane = null;
   }

   /**
    * This method is called from within the constructor to initialize the form.
    * WARNING: Do NOT modify this code. The content of this method is always
    * regenerated by the Form Editor.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents() {

      jPanel1 = new javax.swing.JPanel();
      buttonProfile = new javax.swing.JButton();
      buttonManagement = new javax.swing.JButton();
      buttonUsers = new javax.swing.JButton();
      buttonTasks = new javax.swing.JButton();
      buttonMessages = new javax.swing.JButton();
      buttonFiles = new javax.swing.JButton();

      jPanel1.setBorder(javax.swing.BorderFactory.createCompoundBorder());

      buttonProfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/user.png"))); // NOI18N
      buttonProfile.setText("<html>    <font color=blue size=\"4\"><b>Профиль</b></font><br>    <font color=gray size='2'>       Изменить данные аккаунта<br>       - пароль<br>       - email<br>       - фото<br>    </font> </html>");
      buttonProfile.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
      buttonProfile.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
      buttonProfile.setMinimumSize(new java.awt.Dimension(218, 105));
      buttonProfile.setPreferredSize(new java.awt.Dimension(218, 105));
      buttonProfile.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonProfileActionPerformed(evt);
         }
      });

      buttonManagement.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/management.png"))); // NOI18N
      buttonManagement.setText("<html>    <font color=blue size=\"4\"><b>Менеджмент</b></font><br>    <font color=gray size='2'>       Управление аккаунтами<br>       - Изменить данные<br>       - Сформировать задачу<br>       - Зарегистрировать сотрудника<br>    </font> </html>");
      buttonManagement.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
      buttonManagement.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
      buttonManagement.setPreferredSize(new java.awt.Dimension(247, 105));
      buttonManagement.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonManagementActionPerformed(evt);
         }
      });

      buttonUsers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/collegues.png"))); // NOI18N
      buttonUsers.setText("<html>    <font color=blue size=\"4\"><b>Коллеги</b></font><br>    <font color=gray size='2'>       - Просмотр профилей коллег<br>       - Написать сообщение<br><br><br>    </font> </html>");
      buttonUsers.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
      buttonUsers.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
      buttonUsers.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonUsersActionPerformed(evt);
         }
      });

      buttonTasks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/tasks.png"))); // NOI18N
      buttonTasks.setText("<html>    <font color=blue size=\"4\"><b>Задачи</b></font><br>    <font color=gray size='2'>       - Просмотр ваших задач<br>       - Завершить задачу<br><br><br>    </font> </html>");
      buttonTasks.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
      buttonTasks.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
      buttonTasks.setPreferredSize(new java.awt.Dimension(277, 105));
      buttonTasks.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonTasksActionPerformed(evt);
         }
      });

      buttonMessages.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/messages.png"))); // NOI18N
      buttonMessages.setText("<html>    <font color=blue size=\"4\"><b>Сообщения</b></font><br>    <font color=gray size='2'>       Архив ваших сообщений<br>       - Входящие<br>       - Исходящие<br><br>    </font> </html>");
      buttonMessages.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
      buttonMessages.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
      buttonMessages.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonMessagesActionPerformed(evt);
         }
      });

      buttonFiles.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/files.png"))); // NOI18N
      buttonFiles.setText("<html>    <font color=blue size=\"4\"><b>Файлы</b></font><br>    <font color=gray size='2'>       Архив всех файлов<br>       - Скачать файл<br>       - Выгрузить свой файл<br><br>    </font> </html>");
      buttonFiles.setHorizontalAlignment(javax.swing.SwingConstants.LEADING);
      buttonFiles.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
      buttonFiles.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonFilesActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
      jPanel1.setLayout(jPanel1Layout);
      jPanel1Layout.setHorizontalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
               .addComponent(buttonProfile, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
               .addComponent(buttonMessages))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(buttonUsers, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonFiles, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
               .addComponent(buttonManagement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addComponent(buttonTasks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );
      jPanel1Layout.setVerticalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(buttonProfile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addComponent(buttonUsers, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonTasks, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(buttonMessages, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonManagement, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonFiles, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
      );

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
      this.setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap(34, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(34, Short.MAX_VALUE))
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap(14, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(14, Short.MAX_VALUE))
      );
   }// </editor-fold>//GEN-END:initComponents

   public static void showSelfProfile() {
      openIfAbsent(" Мой профиль ", selfProfilePane);
   }
   
   public static void showUsers() {
      openIfAbsent(" Коллеги ", usersPane);
   }
   
   public static void showTasks() {
      new Thread(()->{
         openIfAbsent(" Мои задачи ", tasksPane);
         tasksPane.requestUpdate();
      }).start();
   }
   
   public static void showMessages() {
      new Thread(()->{
         openIfAbsent(" Мои сообщения ", messagesPane);
         messagesPane.requestUpdate();
      }).start();
   }
   
   public static void showFiles() {
      openIfAbsent(" Файловый архив ", fileStoragePane);
   }
   
   public static void showManagement() {
      openIfAbsent(" Менеджер ", adminPane);
   }
   
   private void buttonProfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonProfileActionPerformed
      showSelfProfile();
   }//GEN-LAST:event_buttonProfileActionPerformed

   private void buttonUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUsersActionPerformed
      showUsers();
   }//GEN-LAST:event_buttonUsersActionPerformed

   private void buttonTasksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonTasksActionPerformed
      showTasks();
   }//GEN-LAST:event_buttonTasksActionPerformed

   private void buttonMessagesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonMessagesActionPerformed
      showMessages();
   }//GEN-LAST:event_buttonMessagesActionPerformed

   private void buttonFilesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonFilesActionPerformed
      showFiles();
   }//GEN-LAST:event_buttonFilesActionPerformed

   private void buttonManagementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonManagementActionPerformed
      showManagement();
   }//GEN-LAST:event_buttonManagementActionPerformed


   // Variables declaration - do not modify//GEN-BEGIN:variables
   private static javax.swing.JButton buttonFiles;
   private static javax.swing.JButton buttonManagement;
   private static javax.swing.JButton buttonMessages;
   private static javax.swing.JButton buttonProfile;
   private static javax.swing.JButton buttonTasks;
   private static javax.swing.JButton buttonUsers;
   private javax.swing.JPanel jPanel1;
   // End of variables declaration//GEN-END:variables
}
