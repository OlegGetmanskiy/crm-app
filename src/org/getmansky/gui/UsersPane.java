/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import org.getmansky.gui.frame.account.ViewUserFrame;
import org.getmansky.helper.Helper;
import org.getmansky.gui.frame.messages.SendMessageFrame;
import org.getmansky.model.Profile;
import org.getmansky.net.Network;
import org.getmansky.net.json.Group;
import org.getmansky.net.json.User;

/**
 *
 * @author OlegusGetman
 */
public class UsersPane extends javax.swing.JPanel {

   private Long selectedUserId;
   private User selectedUser;
   /**
    * Creates new form UsersPane
    */
   public UsersPane() {
      initComponents();
      tableUsers.getTableHeader().setReorderingAllowed(false);
      tableUsers.removeColumn(tableUsers.getColumnModel().getColumn(0));
      tableUsers.removeColumn(tableUsers.getColumnModel().getColumn(0));
      tableUsers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      tableUsers.setAutoCreateRowSorter(true);
      tableUsers.getSelectionModel().addListSelectionListener((ListSelectionEvent e) -> {
         new Thread(()->{
            int selRow = tableUsers.convertRowIndexToModel(tableUsers.getSelectedRow());
            selectedUserId = null;
            if (selRow == -1) {
               return;
            }
            long userId = (long) tableUsers.getModel().getValueAt(selRow, 0);
            try {
               selectedUserId = userId;
               Profile profile = Profile.fromUser(selectedUser = Network.user(selectedUserId));
               labelPhone.setText(selectedUser.getPhone());
               textEmail.setText(selectedUser.getEmail());
               if (profile.getPhoto() != null) {
                  labelPhoto.setIcon(new ImageIcon(profile.getPhoto()));
                  labelPhoto.setText("");
               } else {
                  labelPhoto.setIcon(null);
                  labelPhoto.setText("Нет фото");
               }
               //new ViewUserFrame(selectedUser);
            } catch (IOException ex) {
               Logger.getLogger(AdminPane.class.getName()).log(Level.SEVERE, null, ex);
            }
         }).start();
      });
      setupKeyboardShortcuts();
      loadUsersList();
   }
   
   private void setupKeyboardShortcuts() {
      InputMap im = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
      ActionMap am = getActionMap();
      
      im.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "refresh");
      am.put("refresh", new AbstractAction() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if(buttonUpdateUsersList.isEnabled())
               buttonUpdateUsersListActionPerformed(null);
         }
      });
   }
   
   final void loadUsersList() {
      buttonUpdateUsersList.setEnabled(false);
      new Thread(() -> {
         DefaultTableModel model = (DefaultTableModel) tableUsers.getModel();
         model.setRowCount(0);
         try {
            User[] users = Network.users().getUsers();
            List<Group> groups = Network.groups().getGroups();
            for (User user : users) {
               model.addRow(new Object[]{
                  user.getId(),
                  user.getGroupId(),
                  user.getFname() + ' ' + user.getName() + ' ' + user.getOname(),
                  user.getPosition(),
                  groups.stream().filter(group -> group.getId() == user.getGroupId()).findFirst().get().getTitle()
               });
            }
         } catch (IOException e) {
            Helper.showMessage("Ошибка сетевого запроса");
         } finally {
            buttonUpdateUsersList.setEnabled(true);
         }
      }).start();
   }

   /**
    * This method is called from within the constructor to initialize the form.
    * WARNING: Do NOT modify this code. The content of this method is always
    * regenerated by the Form Editor.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents() {

      jScrollPane2 = new javax.swing.JScrollPane();
      tableUsers = new javax.swing.JTable();
      buttonUpdateUsersList = new javax.swing.JButton();
      labelPhoto = new javax.swing.JLabel();
      buttonSendMessage = new javax.swing.JButton();
      labelPhone = new javax.swing.JLabel();
      textEmail = new javax.swing.JTextField();

      tableUsers.setModel(new javax.swing.table.DefaultTableModel(
         new Object [][] {

         },
         new String [] {
            "id", "groupId", "ФИО", "Должность", "Группа"
         }
      ) {
         Class[] types = new Class [] {
            java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
         };
         boolean[] canEdit = new boolean [] {
            false, false, false, false, false
         };

         public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
         }

         public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
         }
      });
      tableUsers.setToolTipText("");
      tableUsers.addMouseListener(new java.awt.event.MouseAdapter() {
         public void mouseClicked(java.awt.event.MouseEvent evt) {
            tableUsersMouseClicked(evt);
         }
         public void mouseReleased(java.awt.event.MouseEvent evt) {
            tableUsersMouseReleased(evt);
         }
      });
      jScrollPane2.setViewportView(tableUsers);

      buttonUpdateUsersList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/retry.png"))); // NOI18N
      buttonUpdateUsersList.setText("<html>    Обновить<br>    <font color=gray size='2'>       Перезагрузить весь список    </font> </html>");
      buttonUpdateUsersList.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonUpdateUsersListActionPerformed(evt);
         }
      });

      labelPhoto.setBackground(new java.awt.Color(204, 204, 204));
      labelPhoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
      labelPhoto.setText("<html><font color=gray>Выберите коллегу</font></html>");
      labelPhoto.setPreferredSize(new java.awt.Dimension(50, 50));

      buttonSendMessage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/email.png"))); // NOI18N
      buttonSendMessage.setText("<html>    Написать<br>    <font color=gray size='2'>       Личное сообщение    </font> </html>");
      buttonSendMessage.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
      buttonSendMessage.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonSendMessageActionPerformed(evt);
         }
      });

      labelPhone.setText("   ");

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
      this.setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(buttonUpdateUsersList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addGroup(layout.createSequentialGroup()
                  .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(labelPhone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                           .addComponent(labelPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                           .addComponent(buttonSendMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                           .addComponent(textEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 151, Short.MAX_VALUE)))))
            .addContainerGap())
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
               .addGroup(layout.createSequentialGroup()
                  .addComponent(labelPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonSendMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(labelPhone)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(textEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, Short.MAX_VALUE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(buttonUpdateUsersList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
      );
   }// </editor-fold>//GEN-END:initComponents

   private void tableUsersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableUsersMouseClicked
      if(evt.getClickCount() == 2) {
         if (selectedUserId != null) {
            new Thread(()->{
               new ViewUserFrame(selectedUserId);
            }).start();
         }
      }
   }//GEN-LAST:event_tableUsersMouseClicked

   private void buttonUpdateUsersListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUpdateUsersListActionPerformed
      Network.cachedUsers.clear();
      loadUsersList();
   }//GEN-LAST:event_buttonUpdateUsersListActionPerformed

   private void buttonSendMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSendMessageActionPerformed
      if(selectedUserId != null) {
         new SendMessageFrame(selectedUser);
      } else {
         Helper.showMessage("Сначала выберите коллегу");
      }
   }//GEN-LAST:event_buttonSendMessageActionPerformed

   private void tableUsersMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableUsersMouseReleased
      JMenuItem miProfile = new JMenuItem("Профиль");
      miProfile.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (selectedUserId != null) {
               new Thread(() -> {
                  new ViewUserFrame(selectedUserId);
               }).start();
            }
         }
      });
      JPopupMenu popup = new JPopupMenu();
      popup.add(miProfile);
      Helper.showTableContextMenu(evt, tableUsers, popup);
   }//GEN-LAST:event_tableUsersMouseReleased


   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.JButton buttonSendMessage;
   private javax.swing.JButton buttonUpdateUsersList;
   private javax.swing.JScrollPane jScrollPane2;
   private javax.swing.JLabel labelPhone;
   private javax.swing.JLabel labelPhoto;
   private javax.swing.JTable tableUsers;
   private javax.swing.JTextField textEmail;
   // End of variables declaration//GEN-END:variables
}
