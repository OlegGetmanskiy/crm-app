/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.getmansky.ReportFrame;
import org.getmansky.helper.Helper;
import org.getmansky.gui.frame.account.SelfProfileInfo;
import org.getmansky.gui.frame.tasks.ViewTaskFrame;
import org.getmansky.helper.InformativeDateFormat;
import org.getmansky.model.ModelledTask;
import org.getmansky.net.Network;
import org.getmansky.net.json.Task;
import org.getmansky.net.json.TaskCategory;
import org.getmansky.net.json.User;

/**
 *
 * @author OlegusGetman
 */
public class TasksPane extends javax.swing.JPanel {
   
   private static List<Task> tasks;
   private static TaskCategory taskCategory;
   private static boolean viewOnlyLive = true;
   private static boolean viewAll = false;
   private static boolean viewAsAuthor = false;
   /**
    * Creates new form TasksPane
    */
   public TasksPane() {
      initComponents();
      if(SelfProfileInfo.isAdminOrManager()) {
         viewAsAuthor = true;
         viewOnlyLive = false;
         buttonGroup2.setSelected(jRadioButton2.getModel(), true);
         
         viewOnlyLive = true;
         comboCategories.setEnabled(radioViewCategories.isSelected());
         //loadMyTasks(true, 0);
      } else {
         buttonGroup2.setSelected(jRadioButton1.getModel(), true);
         comboCategories.setEnabled(false);
      }
      
      tableTasks.getTableHeader().setReorderingAllowed(false);
      tableTasks.removeColumn(tableTasks.getColumnModel().getColumn(0));
      tableTasks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      tableTasks.setAutoCreateRowSorter(true);
      TableRowSorter sorter = (TableRowSorter) tableTasks.getRowSorter();
      sorter.setSortable(3, false);
      sorter.setSortable(4, false);
      sorter.setComparator(1, new Comparator<String>() {
         @Override
         public int compare(String o1, String o2) {
            return Helper.escapeFromHtml(o1).compareTo(Helper.escapeFromHtml(o2));
         }
      });
      sorter.setComparator(2, new Comparator<String>() {
         @Override
         public int compare(String o1, String o2) {
            return Helper.escapeFromHtml(o1).compareTo(Helper.escapeFromHtml(o2));
         }
      });
      sorter.setComparator(4, new Comparator<String>() {
         @Override
         public int compare(String o1, String o2) {
            return Helper.escapeFromHtml(o1).compareTo(Helper.escapeFromHtml(o2));
         }
      });
      sorter.setComparator(6, new Comparator<String>() {
         @Override
         public int compare(String o1, String o2) {
            return Helper.escapeFromHtml(o1).compareTo(Helper.escapeFromHtml(o2));
         }
      });
      
      buttonGroup1.setSelected(radioViewAll.getModel(), true);
      jRadioButton2.setEnabled(SelfProfileInfo.isAdminOrManager());
      buttonReportEmployees.setVisible(SelfProfileInfo.isAdminOrManager());
      
      updateCategories();
      loadMyTasks(true, 0);
      setupKeyboardShortcuts();
   }
   
   private void setupKeyboardShortcuts() {
      InputMap im = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
      ActionMap am = getActionMap();
      
      im.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "refresh");
      am.put("refresh", new AbstractAction() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if(buttonUpdateTasks.isEnabled())
               updateCategories();
         }
      });
   }
   
   public final void updateCategories() {
      new Thread(()->{
         try {
            buttonUpdateTasks.setEnabled(false);
            buttonLoadMore.setEnabled(false);
            comboCategories.removeAllItems();
            for (TaskCategory cat : Network.myTaskCategories().getTaskCategories()) {
               comboCategories.addItem(cat);
            }
            taskCategory = ((TaskCategory) comboCategories.getItemAt(0));
         } catch (IOException e) {
            e.printStackTrace();
         }
      }).start();
   }
   
   public static void updateInfo() {
      
   }
   
   public static void loadMyTasks(boolean clean, long offset) {
      String boldPrepend = "<html><b>";
      String boldAppend = "</b></html>";
      String redPrepend = "<html><font color='red'>";
      String redAppend = "</font></html>";
      String greenPrepend = "<html><font color='green'>";
      String greenAppend = "</font></html>";
      String yellowPrepend = "<html><font color=#DAA520>";
      String yellowAppend = "</font></html>";
      String grayPrepend = "<html><font color='#9C9C9C'>";
      String grayAppend = "</font></html>";
      if(taskCategory == null) return;
      new Thread(()->{
         buttonUpdateTasks.setEnabled(false);
         buttonLoadMore.setEnabled(false);
         DefaultTableModel model = (DefaultTableModel) tableTasks.getModel();
         if(clean) model.setRowCount(0);
         String append;
         String prepend;
         try {
            Task[] tasks = null;
            viewAsAuthor = jRadioButton2.isSelected();
            viewOnlyLive = radioViewLive.isSelected();
            viewAll = radioViewAll.isSelected();
            if(viewAsAuthor) {
               User user = new User();
               user.setId(SelfProfileInfo.id);
               if (viewOnlyLive) {
                  tasks = Network.activeTasksByAuthor(user, offset).getTasks();
               } else if(viewAll) {
                  tasks = Network.tasksByAuthor(user, offset).getTasks();
               } else {
                  tasks = Network.tasksByAuthor(user, taskCategory, offset).getTasks();
               }
            } else {
               if (viewOnlyLive) {
                  tasks = Network.myLiveTasks(offset);
               } else if(viewAll) {
                  User user = new User();
                  user.setId(SelfProfileInfo.id);
                  tasks = Network.tasksByUser(user, offset).getTasks();
               } else {
                  tasks = Network.myTasks(taskCategory, offset).getTasks();
               }
            }
            Helper.unsortTable(tableTasks);
            
            TableRowSorter sorter = (TableRowSorter) tableTasks.getRowSorter();
            sorter.setSortable(3, false);
            sorter.setSortable(4, false);
            for(Task task : tasks) {
               ModelledTask m = ModelledTask.fromJsonTask(task);
               User empl = m.getEmployee();
               String fio = empl.getFname() + ' ' + empl.getName() + ' ' + empl.getOname();
               Object[] row;
               if(!m.isRead()) {
                  if (m.isDead()) {
                     prepend = redPrepend;
                     append = redAppend;
                  } else {
                     if(m.isComplete()) {
                        prepend = grayPrepend;
                        append = grayAppend;
                     } else {
                        if (InformativeDateFormat.daysBetween(new Date(), m.getEndDate()) <= 3) {
                           prepend = yellowPrepend;
                           append = yellowAppend;
                        } else {
                           prepend = greenPrepend;
                           append = greenAppend;
                        }
                     }
                  }
                  prepend = prepend + "<b>";
                  append = "</b>" + append;
                  row = new Object[]{
                     m.getId(),
                     prepend + m.getTitle() + append,
                     prepend + fio + append,
                     prepend + Helper.dateFormat.format(m.getStartDate()) + append,
                     prepend + Helper.dateFormat.format(m.getEndDate()) + append,
                     prepend + m.getPriority() + append,
                     m.isComplete(),
                     prepend + m.getCategory() + append
                  };
               } else {
                  prepend = "";
                  append = "";
                  if(m.isDead()) {
                     prepend = redPrepend;
                     append = redAppend;
                  } else {
                     if (m.isComplete()) {
                        prepend = grayPrepend;
                        append = grayAppend;
                     } else {
                        if (InformativeDateFormat.daysBetween(new Date(), m.getEndDate()) <= 3) {
                           prepend = yellowPrepend;
                           append = yellowAppend;
                        } else {
                           prepend = greenPrepend;
                           append = greenAppend;
                        }
                     }
                  }
                  row = new Object[]{
                     m.getId(),
                     prepend + m.getTitle() + append,
                     prepend + fio + append,
                     prepend + Helper.dateFormat.format(m.getStartDate()) + append,
                     prepend + Helper.dateFormat.format(m.getEndDate()) + append,
                     prepend + m.getPriority() + append,
                     m.isComplete(),
                     prepend + m.getCategory() + append
                  };
               }
               model.addRow(row);
            }
         } catch(IOException e) {
            Helper.showMessage("Ошибка сетевого запроса");
            e.printStackTrace();
         } catch(NullPointerException e) {
            //do nothing
         } finally {
            buttonUpdateTasks.setEnabled(true);
            buttonLoadMore.setEnabled(true);
         }
      }).start();
   }
   
   public void requestUpdate() {
      updateCategories();
      //loadMyTasks(true, 0);
   }

   /**
    * This method is called from within the constructor to initialize the form.
    * WARNING: Do NOT modify this code. The content of this method is always
    * regenerated by the Form Editor.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents() {

      buttonGroup1 = new javax.swing.ButtonGroup();
      buttonGroup2 = new javax.swing.ButtonGroup();
      buttonGroup3 = new javax.swing.ButtonGroup();
      jPanel2 = new javax.swing.JPanel();
      jScrollPane2 = new javax.swing.JScrollPane();
      tableTasks = new javax.swing.JTable();
      jPanel1 = new javax.swing.JPanel();
      radioViewLive = new javax.swing.JRadioButton();
      comboCategories = new javax.swing.JComboBox();
      jRadioButton1 = new javax.swing.JRadioButton();
      jRadioButton2 = new javax.swing.JRadioButton();
      jLabel1 = new javax.swing.JLabel();
      jSeparator1 = new javax.swing.JSeparator();
      radioViewAll = new javax.swing.JRadioButton();
      radioViewCategories = new javax.swing.JRadioButton();
      buttonLoadMore = new javax.swing.JButton();
      buttonUpdateTasks = new javax.swing.JButton();
      buttonReportEmployees = new javax.swing.JButton();

      addKeyListener(new java.awt.event.KeyAdapter() {
         public void keyPressed(java.awt.event.KeyEvent evt) {
            formKeyPressed(evt);
         }
      });

      jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Ваши задачи"));

      tableTasks.setModel(new javax.swing.table.DefaultTableModel(
         new Object [][] {

         },
         new String [] {
            "id", "Название", "Кому выдана", "Поставлена", "Крайний срок", "Приоритет", "Завершена", "Категория"
         }
      ) {
         Class[] types = new Class [] {
            java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.String.class
         };
         boolean[] canEdit = new boolean [] {
            false, false, false, false, false, false, false, false
         };

         public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
         }

         public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
         }
      });
      tableTasks.setToolTipText("");
      tableTasks.setSelectionBackground(new java.awt.Color(204, 255, 255));
      tableTasks.addMouseListener(new java.awt.event.MouseAdapter() {
         public void mouseClicked(java.awt.event.MouseEvent evt) {
            tableTasksMouseClicked(evt);
         }
         public void mouseReleased(java.awt.event.MouseEvent evt) {
            tableTasksMouseReleased(evt);
         }
      });
      jScrollPane2.setViewportView(tableTasks);
      if (tableTasks.getColumnModel().getColumnCount() > 0) {
         tableTasks.getColumnModel().getColumn(1).setPreferredWidth(350);
         tableTasks.getColumnModel().getColumn(2).setPreferredWidth(150);
         tableTasks.getColumnModel().getColumn(3).setPreferredWidth(100);
         tableTasks.getColumnModel().getColumn(4).setPreferredWidth(100);
      }

      jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Режим просмотра"));

      buttonGroup1.add(radioViewLive);
      radioViewLive.setText("Только текущие");
      radioViewLive.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            radioViewLiveActionPerformed(evt);
         }
      });

      comboCategories.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            comboCategoriesActionPerformed(evt);
         }
      });

      buttonGroup2.add(jRadioButton1);
      jRadioButton1.setSelected(true);
      jRadioButton1.setText("Выданные мне");
      jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jRadioButton1ActionPerformed(evt);
         }
      });

      buttonGroup2.add(jRadioButton2);
      jRadioButton2.setText("Выданные мною");
      jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jRadioButton2ActionPerformed(evt);
         }
      });

      jLabel1.setText("Принадлежность");

      buttonGroup1.add(radioViewAll);
      radioViewAll.setSelected(true);
      radioViewAll.setText("Все");
      radioViewAll.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            radioViewAllActionPerformed(evt);
         }
      });

      buttonGroup1.add(radioViewCategories);
      radioViewCategories.setText("По категориям");
      radioViewCategories.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            radioViewCategoriesActionPerformed(evt);
         }
      });
      radioViewCategories.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
         public void propertyChange(java.beans.PropertyChangeEvent evt) {
            radioViewCategoriesPropertyChange(evt);
         }
      });

      javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
      jPanel1.setLayout(jPanel1Layout);
      jPanel1Layout.setHorizontalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(jSeparator1)
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addComponent(jLabel1)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(jRadioButton1)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(jRadioButton2)
                  .addGap(0, 188, Short.MAX_VALUE))
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addComponent(radioViewAll)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(radioViewLive)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(radioViewCategories)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(comboCategories, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addContainerGap())
      );
      jPanel1Layout.setVerticalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(jRadioButton1)
               .addComponent(jRadioButton2)
               .addComponent(jLabel1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(comboCategories, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                  .addComponent(radioViewAll)
                  .addComponent(radioViewLive)
                  .addComponent(radioViewCategories)))
            .addContainerGap())
      );

      buttonLoadMore.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/more.png"))); // NOI18N
      buttonLoadMore.setText("<html>    Загрузить еще<br>    <font color=gray size='2'>       Следующие 50 записей    </font> </html>");
      buttonLoadMore.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
      buttonLoadMore.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonLoadMoreActionPerformed(evt);
         }
      });

      buttonUpdateTasks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/retry.png"))); // NOI18N
      buttonUpdateTasks.setText("<html>    Обновить<br>    <font color=gray size='2'>       Перезагрузить весь список    </font> </html>");
      buttonUpdateTasks.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
      buttonUpdateTasks.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonUpdateTasksActionPerformed(evt);
         }
      });

      buttonReportEmployees.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/newtask.png"))); // NOI18N
      buttonReportEmployees.setText("<html>    Все текущие задачи<br>    <font color=gray size='2'>Отчет по текущим задачам сотрудников</font> </html>");
      buttonReportEmployees.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
      buttonReportEmployees.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonReportEmployeesActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
      jPanel2.setLayout(jPanel2Layout);
      jPanel2Layout.setHorizontalGroup(
         jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(jScrollPane2)
               .addGroup(jPanel2Layout.createSequentialGroup()
                  .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(buttonUpdateTasks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(buttonLoadMore, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                     .addComponent(buttonReportEmployees, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
      );
      jPanel2Layout.setVerticalGroup(
         jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanel2Layout.createSequentialGroup()
                  .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                     .addComponent(buttonLoadMore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                     .addComponent(buttonUpdateTasks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonReportEmployees, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addGroup(jPanel2Layout.createSequentialGroup()
                  .addContainerGap()
                  .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
            .addContainerGap())
      );

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
      this.setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap())
      );
   }// </editor-fold>//GEN-END:initComponents

   private void buttonUpdateTasksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUpdateTasksActionPerformed
      radioViewAll.setSelected(true);
      updateCategories();
   }//GEN-LAST:event_buttonUpdateTasksActionPerformed

   private void tableTasksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTasksMouseClicked
      if(evt.getClickCount() == 2) {
         int row = tableTasks.convertRowIndexToModel(tableTasks.getSelectedRow());
         User user = new User();
         user.setId(SelfProfileInfo.id);
         
         Task task = new Task();
         task.setId((Long)tableTasks.getModel().getValueAt(row, 0));
         System.out.println("taskId: " + task.getId());
         
         ViewTaskFrame viewTaskFrame = new ViewTaskFrame(task, user, true);
      }
   }//GEN-LAST:event_tableTasksMouseClicked

   private void comboCategoriesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategoriesActionPerformed
      taskCategory = (TaskCategory) comboCategories.getSelectedItem();
      loadMyTasks(true, 0);
   }//GEN-LAST:event_comboCategoriesActionPerformed

   private void buttonLoadMoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLoadMoreActionPerformed
      loadMyTasks(false, tableTasks.getRowCount());
   }//GEN-LAST:event_buttonLoadMoreActionPerformed

   private void radioViewCategoriesPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_radioViewCategoriesPropertyChange
      //nothing
   }//GEN-LAST:event_radioViewCategoriesPropertyChange

   private void radioViewCategoriesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioViewCategoriesActionPerformed
      viewOnlyLive = false;
      comboCategories.setEnabled(radioViewCategories.isSelected());
      loadMyTasks(true, 0);
   }//GEN-LAST:event_radioViewCategoriesActionPerformed

   private void radioViewLiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioViewLiveActionPerformed
      viewOnlyLive = true;
      comboCategories.setEnabled(radioViewCategories.isSelected());
      loadMyTasks(true, 0);
   }//GEN-LAST:event_radioViewLiveActionPerformed

   private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
      viewAsAuthor = false;
      loadMyTasks(true, 0);
   }//GEN-LAST:event_jRadioButton1ActionPerformed

   private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
      viewAsAuthor = true;
      loadMyTasks(true, 0);
   }//GEN-LAST:event_jRadioButton2ActionPerformed

   private void buttonReportEmployeesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonReportEmployeesActionPerformed
      new Thread(()->{
         String origText = buttonReportEmployees.getText();
         buttonReportEmployees.setText("...");
         buttonReportEmployees.setEnabled(false);
         try {
            new ReportFrame(Network.employeesReport());
         } catch (IOException ex) {
            Helper.showMessage("Ошибка запроса: " + ex.getMessage());
         } finally {
            buttonReportEmployees.setText(origText);
            buttonReportEmployees.setEnabled(true);
         }
      }).start();
   }//GEN-LAST:event_buttonReportEmployeesActionPerformed

   private void tableTasksMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTasksMouseReleased
      JMenuItem miProfile = new JMenuItem("Задача");
      miProfile.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            int row = tableTasks.getSelectedRow();
            User user = new User();
            user.setId(SelfProfileInfo.id);

            Task task = new Task();
            task.setId((Long) tableTasks.getModel().getValueAt(row, 0));

            ViewTaskFrame viewTaskFrame = new ViewTaskFrame(task, user, true);
         }
      });
      JPopupMenu popup = new JPopupMenu();
      popup.add(miProfile);
      Helper.showTableContextMenu(evt, tableTasks, popup);
   }//GEN-LAST:event_tableTasksMouseReleased

   private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
      
   }//GEN-LAST:event_formKeyPressed

   private void radioViewAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioViewAllActionPerformed
      viewOnlyLive = false;
      comboCategories.setEnabled(radioViewCategories.isSelected());
      loadMyTasks(true, 0);
   }//GEN-LAST:event_radioViewAllActionPerformed


   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.ButtonGroup buttonGroup1;
   private javax.swing.ButtonGroup buttonGroup2;
   private javax.swing.ButtonGroup buttonGroup3;
   private static javax.swing.JButton buttonLoadMore;
   private javax.swing.JButton buttonReportEmployees;
   private static javax.swing.JButton buttonUpdateTasks;
   private javax.swing.JComboBox comboCategories;
   private javax.swing.JLabel jLabel1;
   private javax.swing.JPanel jPanel1;
   private javax.swing.JPanel jPanel2;
   private static javax.swing.JRadioButton jRadioButton1;
   private static javax.swing.JRadioButton jRadioButton2;
   private javax.swing.JScrollPane jScrollPane2;
   private javax.swing.JSeparator jSeparator1;
   private static javax.swing.JRadioButton radioViewAll;
   private static javax.swing.JRadioButton radioViewCategories;
   private static javax.swing.JRadioButton radioViewLive;
   private static javax.swing.JTable tableTasks;
   // End of variables declaration//GEN-END:variables
}
