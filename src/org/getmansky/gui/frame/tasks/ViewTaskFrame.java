/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.gui.frame.tasks;

import org.getmansky.gui.frame.messages.SendMessageFrame;
import org.getmansky.gui.frame.account.SelfProfileInfo;
import java.awt.Component;
import org.getmansky.helper.Helper;
import java.io.IOException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import org.getmansky.gui.TasksPane;
import org.getmansky.gui.frame.files.AttachedFilesDialog;
import org.getmansky.model.Profile;
import org.getmansky.net.Network;
import org.getmansky.net.json.Task;
import org.getmansky.net.json.TaskPriority;
import org.getmansky.net.json.User;
import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 *
 * @author OlegusGetman
 */
public class ViewTaskFrame extends javax.swing.JFrame {
   
   private Task task;
   private User user;
   private User author;
   
   private boolean postponeByDays = false;
   
   /**
    * Creates new form ViewTaskFrame
    * @param task
    * @param user
    */
   public ViewTaskFrame(Task task, User user, boolean editable) {      
      initComponents();
      
      buttonCompleteTask.setEnabled(editable);
      buttonUncompleteTask.setEnabled(editable);
      
      Calendar c = Calendar.getInstance();
      c.add(Calendar.YEAR, 5);
      
      setLocationRelativeTo(null);
      setIconImage(Helper.windowIcon);
      setVisible(true);
      
      this.task = task;
      this.user = user;
      
      new Thread(()->{
         try {
            Task t = Network.task(user, task);
            this.task = t;
            
            dateDeadline.setSelectableDateRange(t.getEndDate(), c.getTime());
            dateDeadline.setDate(task.getEndDate());
            labelStart.setText("Дата постановки задачи: "+Helper.dateFormat.format(t.getStartDate()));
            if(t.isComplete()) {
               for(Component comp : panelManager.getComponents()) {
                  comp.setEnabled(false);
               }
               int days = Days.daysBetween(new DateTime(t.getStartDate()), new DateTime(t.getCompleteDate())).getDays();
               if(days > 0)  {
                  int lateDays = t.getLateDays();
                  if(lateDays <= 0)
                     labelDeadline.setText("Задача выполнена за "+days+" дней");
                  else {
                     labelDeadline.setText("Задача выполнена за "+days+" дней, с опозданием на " + lateDays + " дней");
                  }
               }
               else if(days == 0) 
                  labelDeadline.setText("Задача выполнена в тот же день, в который выдана");
            } else {
               int lateDays = t.getLateDays();
               if (lateDays <= 0) {
                  labelDeadline.setText("Крайний срок выполнения: " + Helper.dateFormat.format(t.getEndDate()));
               } else {
                  labelDeadline.setText("Крайний срок выполнения: " + Helper.dateFormat.format(t.getEndDate()) + " (опоздание на " + lateDays + " дней)");
               }
            }
            setTitle("Задача: "+t.getTitle());
            
            buttonCompleteTask.setEnabled(!t.isComplete());
            buttonUncompleteTask.setEnabled(t.isComplete());
            author = Network.user(t.getAuthorId());
            author.setId(t.getAuthorId());
            labelAuthorName.setText(author.getFname() + ' ' + author.getName() + ' ' + author.getOname());
            textTaskDesc.setText(t.getDescription());
            labelPhoto.setIcon(new ImageIcon(Profile.fromUser(author).getPhoto()));
            
            buttonViewAttachedFiles.setEnabled(t.getAttachedFiles() != null);
            if(t.getAttachedFiles() != null) {
               buttonViewAttachedFiles.setText("Прикрепленные файлы ("+t.getAttachedFiles().length+")");
            }
            
            comboPriority.removeAllItems();
            TaskPriority[] ps = Network.taskPriorities().getPriorities();
            for(TaskPriority p : ps) {
               comboPriority.addItem(p);
            }
         } catch (IOException ex) {
            Helper.showMessage("Ошибка запроса: "+ex.getMessage());
            ex.printStackTrace();
            dispose();
         }
      }).start();
      
      if(!SelfProfileInfo.isAdminOrManager()) {
         this.remove(panelManager);
      }
   }

   /**
    * This method is called from within the constructor to initialize the form.
    * WARNING: Do NOT modify this code. The content of this method is always
    * regenerated by the Form Editor.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents() {

      buttonGroup1 = new javax.swing.ButtonGroup();
      jScrollPane1 = new javax.swing.JScrollPane();
      textTaskDesc = new javax.swing.JTextArea();
      buttonExit = new javax.swing.JButton();
      buttonCompleteTask = new javax.swing.JButton();
      buttonUncompleteTask = new javax.swing.JButton();
      panelManager = new javax.swing.JPanel();
      jLabel1 = new javax.swing.JLabel();
      dateDeadline = new com.toedter.calendar.JDateChooser();
      buttonPostpone = new javax.swing.JButton();
      jRadioButton1 = new javax.swing.JRadioButton();
      jRadioButton2 = new javax.swing.JRadioButton();
      comboCalendar = new javax.swing.JComboBox();
      textDays = new javax.swing.JSpinner();
      buttonEditTask = new javax.swing.JButton();
      comboPriority = new javax.swing.JComboBox();
      jLabel2 = new javax.swing.JLabel();
      buttonSetPriority = new javax.swing.JButton();
      jPanel1 = new javax.swing.JPanel();
      labelAuthorName = new javax.swing.JLabel();
      labelPhoto = new javax.swing.JLabel();
      buttonSendMessage = new javax.swing.JButton();
      labelDeadline = new javax.swing.JLabel();
      buttonViewAttachedFiles = new javax.swing.JButton();
      labelStart = new javax.swing.JLabel();

      setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
      setTitle("Задача");

      textTaskDesc.setColumns(20);
      textTaskDesc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
      textTaskDesc.setLineWrap(true);
      textTaskDesc.setRows(5);
      textTaskDesc.setWrapStyleWord(true);
      jScrollPane1.setViewportView(textTaskDesc);

      buttonExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/delete.png"))); // NOI18N
      buttonExit.setText("<html>    Закрыть<br>    <font color=gray size='2'>       Закрыть это окно   </font> </html>");
      buttonExit.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonExitActionPerformed(evt);
         }
      });

      buttonCompleteTask.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/newtask.png"))); // NOI18N
      buttonCompleteTask.setText("<html>    Завершить задачу<br>    <font color=gray size='2'>       Уведомить о выполнении    </font> </html>");
      buttonCompleteTask.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonCompleteTaskActionPerformed(evt);
         }
      });

      buttonUncompleteTask.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/edit.png"))); // NOI18N
      buttonUncompleteTask.setText("<html>    Задача в работе<br>    <font color=gray size='2'>       Вернуть задачу в работу    </font> </html>");
      buttonUncompleteTask.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonUncompleteTaskActionPerformed(evt);
         }
      });

      panelManager.setBorder(javax.swing.BorderFactory.createTitledBorder("Менеджер"));

      jLabel1.setText("Отложить крайний срок");

      buttonPostpone.setText("Отложить");
      buttonPostpone.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonPostponeActionPerformed(evt);
         }
      });

      buttonGroup1.add(jRadioButton1);
      jRadioButton1.setSelected(true);
      jRadioButton1.setText("До даты");
      jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jRadioButton1ActionPerformed(evt);
         }
      });

      buttonGroup1.add(jRadioButton2);
      jRadioButton2.setText("На время");
      jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jRadioButton2ActionPerformed(evt);
         }
      });

      comboCalendar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "дней", "недель", "месяцев" }));

      textDays.setModel(new javax.swing.SpinnerNumberModel(1, 1, 1000, 1));

      buttonEditTask.setText("Редактировать текст задачи");
      buttonEditTask.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonEditTaskActionPerformed(evt);
         }
      });

      comboPriority.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

      jLabel2.setText("Приоритет");

      buttonSetPriority.setText("Изменить приоритет");
      buttonSetPriority.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonSetPriorityActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout panelManagerLayout = new javax.swing.GroupLayout(panelManager);
      panelManager.setLayout(panelManagerLayout);
      panelManagerLayout.setHorizontalGroup(
         panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelManagerLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(panelManagerLayout.createSequentialGroup()
                  .addGroup(panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(jRadioButton2)
                     .addComponent(jRadioButton1))
                  .addGroup(panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                     .addGroup(panelManagerLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(buttonPostpone))
                     .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelManagerLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(textDays, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboCalendar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 1, Short.MAX_VALUE))
                     .addGroup(panelManagerLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dateDeadline, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
               .addComponent(buttonEditTask, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addGroup(panelManagerLayout.createSequentialGroup()
                  .addComponent(jLabel1)
                  .addGap(0, 0, Short.MAX_VALUE))
               .addGroup(panelManagerLayout.createSequentialGroup()
                  .addComponent(jLabel2)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(buttonSetPriority, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addComponent(comboPriority, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addContainerGap())
      );
      panelManagerLayout.setVerticalGroup(
         panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(panelManagerLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jLabel1)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(dateDeadline, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jRadioButton1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(jRadioButton2)
               .addComponent(comboCalendar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(textDays, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(buttonPostpone)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(buttonEditTask)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(panelManagerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(comboPriority, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jLabel2))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(buttonSetPriority))
      );

      jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Автор задачи"));

      labelAuthorName.setText("ФИО автора");

      labelPhoto.setText("нет фото");
      labelPhoto.setPreferredSize(new java.awt.Dimension(150, 150));

      buttonSendMessage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/email.png"))); // NOI18N
      buttonSendMessage.setText("<html>    Написать<br>    <font color=gray size='2'>       Личное сообщение    </font> </html>");
      buttonSendMessage.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonSendMessageActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
      jPanel1.setLayout(jPanel1Layout);
      jPanel1Layout.setHorizontalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(labelAuthorName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(labelPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                     .addComponent(buttonSendMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                  .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
      );
      jPanel1Layout.setVerticalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(labelAuthorName)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(labelPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(buttonSendMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );

      labelDeadline.setText("крайний срок выполнения");

      buttonViewAttachedFiles.setText("Прикрепленные файлы");
      buttonViewAttachedFiles.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonViewAttachedFilesActionPerformed(evt);
         }
      });

      labelStart.setText("дата постановки");

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                  .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
                     .addComponent(labelDeadline, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                           .addComponent(buttonViewAttachedFiles)
                           .addComponent(labelStart))
                        .addGap(0, 0, Short.MAX_VALUE)))
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                     .addComponent(panelManager, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                  .addContainerGap())
               .addGroup(layout.createSequentialGroup()
                  .addComponent(buttonExit, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonCompleteTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonUncompleteTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, Short.MAX_VALUE))))
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                  .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(panelManager, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
               .addGroup(layout.createSequentialGroup()
                  .addComponent(labelStart)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(labelDeadline)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(jScrollPane1)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonViewAttachedFiles)
                  .addGap(2, 2, 2)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(buttonUncompleteTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                  .addComponent(buttonExit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addComponent(buttonCompleteTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
      );

      pack();
   }// </editor-fold>//GEN-END:initComponents

   private void buttonExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonExitActionPerformed
      dispose();
   }//GEN-LAST:event_buttonExitActionPerformed

   private void buttonCompleteTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCompleteTaskActionPerformed
      new Thread(()->{
         try {
            Network.completeTask(task, user, true);
            TasksPane.loadMyTasks(true, 0);
            Helper.showMessage("Статус задачи изменен: завершена");
         } catch(IOException ex) {
            Helper.showMessage(ex.getMessage());
         } finally {
            dispose();
         }
      }).start();
   }//GEN-LAST:event_buttonCompleteTaskActionPerformed

   private void buttonUncompleteTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUncompleteTaskActionPerformed
      new Thread(()->{
         try {
            Network.completeTask(task, user, false);
            TasksPane.loadMyTasks(true, 0);
            Helper.showMessage("Статус задачи изменен: в работе");
         } catch(IOException ex) {
            Helper.showMessage(ex.getMessage());
         } finally {
            dispose();
         }
      }).start();
   }//GEN-LAST:event_buttonUncompleteTaskActionPerformed

   private void buttonPostponeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPostponeActionPerformed
      if(postponeByDays) {
         Calendar c = Calendar.getInstance();
         c.setTime(task.getEndDate());
         int daysToAdd = (int)textDays.getValue();
         int valueToAdd = Calendar.DATE;
         switch(comboCalendar.getSelectedIndex()) {
            case 0: valueToAdd = Calendar.DATE; break;
            case 1: valueToAdd = Calendar.WEEK_OF_YEAR; break;
            case 2: valueToAdd = Calendar.MONTH; break;
         }
         c.add(valueToAdd, daysToAdd);
         if(Helper.ask("Отложить до "+Helper.dateFormat.format(c.getTime())+"?")) {
            new Thread(() -> {
               buttonPostpone.setEnabled(false);
               buttonPostpone.setText("...");
               try {
                  Network.postponeTask(task.getId(), c.getTime());
                  buttonPostpone.setEnabled(false);
                  buttonPostpone.setText("Выполнено");
               } catch (IOException ex) {
                  Helper.showMessage("Ошибка запроса: " + ex.getMessage());
                  buttonPostpone.setText("Отложить");
                  buttonPostpone.setEnabled(true);
               }
            }).start();
         }
      } else {
         if (dateDeadline.getDate() == null) {
            Helper.showMessage("Сначала выберите новый крайний срок");
            return;
         }
         if (Helper.ask("Отложить до " + Helper.dateFormat.format(dateDeadline.getDate()) + "?")) {
            new Thread(() -> {
               buttonPostpone.setEnabled(false);
               buttonPostpone.setText("...");
               try {
                  Network.postponeTask(task.getId(), dateDeadline.getDate());
                  buttonPostpone.setEnabled(false);
                  buttonPostpone.setText("Выполнено");
               } catch (IOException ex) {
                  Helper.showMessage("Ошибка запроса: " + ex.getMessage());
                  buttonPostpone.setText("Отложить");
                  buttonPostpone.setEnabled(true);
               }
            }).start();
         }
      }
   }//GEN-LAST:event_buttonPostponeActionPerformed

   private void buttonSendMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSendMessageActionPerformed
      if(author != null) {
         new SendMessageFrame(author);
      }
   }//GEN-LAST:event_buttonSendMessageActionPerformed

   private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
      postponeByDays = false;
      dateDeadline.setEnabled(true);
      textDays.setEnabled(false);
      comboCalendar.setEnabled(false);
   }//GEN-LAST:event_jRadioButton1ActionPerformed

   private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
      postponeByDays = true;
      dateDeadline.setEnabled(false);
      textDays.setEnabled(true);
      comboCalendar.setEnabled(true);
   }//GEN-LAST:event_jRadioButton2ActionPerformed

   private void buttonViewAttachedFilesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonViewAttachedFilesActionPerformed
      new AttachedFilesDialog(task);
   }//GEN-LAST:event_buttonViewAttachedFilesActionPerformed

   private void buttonEditTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEditTaskActionPerformed
      if(Helper.ask("<html>Внести изменения в текст задачи?<br>Сотрудник будет об этом уведомлен</html>")) {
         new Thread(() -> {
            task.setDescription(textTaskDesc.getText());
            buttonEditTask.setEnabled(false);
            buttonEditTask.setText("...");
            try {
               Network.editTask(task);
               buttonEditTask.setText("Изменения сохранены");
            } catch (IOException ex) {
               Helper.showMessage("Ошибка запроса: " + ex.getMessage());
               ex.printStackTrace();
               buttonEditTask.setEnabled(true);
               buttonEditTask.setText("Применить изменения");
            }
         }).start();
      }
   }//GEN-LAST:event_buttonEditTaskActionPerformed

   private void buttonSetPriorityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSetPriorityActionPerformed
      new Thread(()->{
         try {
            Network.priorityTask(task.getId(), (TaskPriority)comboPriority.getSelectedItem());
            Helper.showMessage("Приоритет изменен");
         } catch (IOException ex) {
            ex.printStackTrace();
            Helper.showMessage("Ошибка запроса");
         }
      }).start();
   }//GEN-LAST:event_buttonSetPriorityActionPerformed

   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.JButton buttonCompleteTask;
   private javax.swing.JButton buttonEditTask;
   private javax.swing.JButton buttonExit;
   private javax.swing.ButtonGroup buttonGroup1;
   private javax.swing.JButton buttonPostpone;
   private javax.swing.JButton buttonSendMessage;
   private javax.swing.JButton buttonSetPriority;
   private javax.swing.JButton buttonUncompleteTask;
   private javax.swing.JButton buttonViewAttachedFiles;
   private javax.swing.JComboBox comboCalendar;
   private javax.swing.JComboBox comboPriority;
   private com.toedter.calendar.JDateChooser dateDeadline;
   private javax.swing.JLabel jLabel1;
   private javax.swing.JLabel jLabel2;
   private javax.swing.JPanel jPanel1;
   private javax.swing.JRadioButton jRadioButton1;
   private javax.swing.JRadioButton jRadioButton2;
   private javax.swing.JScrollPane jScrollPane1;
   private javax.swing.JLabel labelAuthorName;
   private javax.swing.JLabel labelDeadline;
   private javax.swing.JLabel labelPhoto;
   private javax.swing.JLabel labelStart;
   private javax.swing.JPanel panelManager;
   private javax.swing.JSpinner textDays;
   private javax.swing.JTextArea textTaskDesc;
   // End of variables declaration//GEN-END:variables
}
