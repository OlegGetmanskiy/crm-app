/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.gui.frame.account;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import org.getmansky.helper.Helper;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.getmansky.gui.frame.messages.DialogFrame;
import org.getmansky.gui.frame.tasks.CreateTaskFrame;
import org.getmansky.gui.frame.tasks.DetailedHistoryFrame;
import org.getmansky.gui.frame.tasks.ViewTaskFrame;
import org.getmansky.model.ModelledTask;
import org.getmansky.model.Profile;
import org.getmansky.net.Network;
import org.getmansky.net.json.Task;
import org.getmansky.net.json.TaskCategory;
import org.getmansky.net.json.User;

/**
 *
 * @author OlegusGetman
 */
public class ViewUserFrame extends javax.swing.JFrame {
   
   private boolean viewByCategories = true;
   private boolean viewAll = false;
   private boolean viewActive = false;
   
   private User user;
   private TaskCategory taskCategory;
   private static Set<Long> open = new HashSet(); 
   
   /**
    * Creates new form ViewUserFrame
    * @param id
    */
   public ViewUserFrame(Long id) {
      initComponents();
      try {
         this.user = Network.user(id);
      } catch (IOException ex) {
         Helper.showMessage("Ошибка при загрузке профиля пользователя");
      }
      
      if(open.contains(user.getId())) {
         dispose();
         open.add(user.getId());
         return;
      }
      open.add(user.getId());
      
      tableTasks.getTableHeader().setReorderingAllowed(false);
      tableTasks.removeColumn(tableTasks.getColumnModel().getColumn(0));
      tableTasks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      
      Profile profile = Profile.fromUser(user);
      fillFromProfile(profile);
      
      viewByCategories = comboViewMode.getSelectedIndex() == 0;
      viewAll = comboViewMode.getSelectedIndex() == 1;
      viewActive = comboViewMode.getSelectedIndex() == 2;

      comboCategories.setEnabled(viewByCategories);
      jLabel2.setEnabled(viewByCategories);

      loadTasks(true, 0);
      updateCategories();
      
      labelIsInactive.setVisible(!user.getActive());
      textEmail.setText(user.getEmail());
      labelPhone.setText(user.getPhone());
      
      buttonCreateTask.setVisible(user.getActive() && SelfProfileInfo.isAdminOrManager());
      buttonReportTasks.setVisible(SelfProfileInfo.isAdminOrManager());
      
      setLocationRelativeTo(null);
      //setExtendedState(JFrame.MAXIMIZED_BOTH);
      setIconImage(Helper.windowIcon);
      setupKeyboardShortcuts();
      setVisible(true);
   }
   
   private void setupKeyboardShortcuts() {
      InputMap im = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
      ActionMap am = getRootPane().getActionMap();

      im.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "refresh");
      am.put("refresh", new AbstractAction() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (buttonUpdateTaskList.isEnabled()) {
               buttonUpdateTaskListActionPerformed(null);
            }
         }
      });
   }
   
   private void fillFromProfile(Profile p) {
      setTitle(p.getName() + " " + p.getFname());
      if(p.getPhoto() != null) {
         labelPhoto.setIcon(new ImageIcon(p.getPhoto()));
      }
      labelNames.setText(p.getFname() + " " + p.getName() + " " + p.getOname());
      labelPosition.setText(p.getPosition());
   }
   
   public final void updateCategories() {
      try {
         comboCategories.removeAllItems();
         for(TaskCategory cat : Network.userTaskCategories(user).getTaskCategories()) {
            comboCategories.addItem(cat);
         }
         taskCategory = ((TaskCategory)comboCategories.getItemAt(0));
      } catch(IOException e) {
         e.printStackTrace();
      }
   }
   
   public final void loadTasks(boolean clean, long offset) {
      if(taskCategory == null) return;
      new Thread(()->{
         DefaultTableModel model = (DefaultTableModel) tableTasks.getModel();
         if(clean)
            model.setRowCount(0);
         try {
            Task[] tasks = null;
            if(viewByCategories) {
               try {
                  tasks = Network.tasksByUser(user, taskCategory, offset).getTasks();
               } catch(NullPointerException ex) {
                  return;
               }
            }
            if(viewAll) {
               tasks = Network.tasksByUser(user, offset).getTasks();
            }
            if(viewActive) {
               tasks = Network.activeTasksByUser(user, offset).getTasks();
            }
            for(Task task : tasks) {
               ModelledTask m = ModelledTask.fromJsonTask(task);
               model.addRow(new Object[] {
                  m.getId(),
                  m.getTitle(),
                  Helper.dateFormat.format(m.getStartDate()),
                  Helper.dateFormat.format(m.getEndDate()),
                  m.getPriority(),
                  m.isComplete()
               });
            }
         } catch(IOException e) {
            Helper.showMessage("Ошибка сетевого запроса");
            e.printStackTrace();
         }
      }).start();
   }
   
   public void tableTasksMenu(MouseEvent e) {
      JPopupMenu popup = new JPopupMenu();
      JMenuItem miTask = new JMenuItem("Задача");
      miTask.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            int row = tableTasks.getSelectedRow();

            Task task = new Task();
            task.setId((Long) tableTasks.getModel().getValueAt(row, 0));

            ViewTaskFrame viewTaskFrame = new ViewTaskFrame(task, user, SelfProfileInfo.isAdminOrManager());
         }
      });
      popup.add(miTask);
      Helper.showTableContextMenu(e, tableTasks, popup);
   }

   /**
    * This method is called from within the constructor to initialize the form.
    * WARNING: Do NOT modify this code. The content of this method is always
    * regenerated by the Form Editor.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents() {

      jLabel1 = new javax.swing.JLabel();
      labelPhoto = new javax.swing.JLabel();
      labelNames = new javax.swing.JLabel();
      labelPosition = new javax.swing.JLabel();
      jScrollPane2 = new javax.swing.JScrollPane();
      tableTasks = new javax.swing.JTable();
      labelIsInactive = new javax.swing.JLabel();
      jPanel1 = new javax.swing.JPanel();
      comboCategories = new javax.swing.JComboBox();
      jLabel2 = new javax.swing.JLabel();
      buttonUpdateTaskList = new javax.swing.JButton();
      jLabel3 = new javax.swing.JLabel();
      comboViewMode = new javax.swing.JComboBox();
      buttonCreateTask = new javax.swing.JButton();
      buttonReportTasks = new javax.swing.JButton();
      labelPhone = new javax.swing.JLabel();
      textEmail = new javax.swing.JTextField();
      jButton1 = new javax.swing.JButton();

      setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
      addWindowListener(new java.awt.event.WindowAdapter() {
         public void windowClosing(java.awt.event.WindowEvent evt) {
            formWindowClosing(evt);
         }
      });

      labelPhoto.setText("Нет фото");

      labelNames.setText("Имя Фамилия Отчество");

      labelPosition.setText("Должность");

      tableTasks.setModel(new javax.swing.table.DefaultTableModel(
         new Object [][] {

         },
         new String [] {
            "id", "Название", "Начало", "Завершение", "Приоритет", "Завершена"
         }
      ) {
         Class[] types = new Class [] {
            java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
         };
         boolean[] canEdit = new boolean [] {
            false, false, false, false, false, false
         };

         public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
         }

         public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
         }
      });
      tableTasks.setToolTipText("");
      tableTasks.addMouseListener(new java.awt.event.MouseAdapter() {
         public void mouseClicked(java.awt.event.MouseEvent evt) {
            tableTasksMouseClicked(evt);
         }
         public void mouseReleased(java.awt.event.MouseEvent evt) {
            tableTasksMouseReleased(evt);
         }
      });
      jScrollPane2.setViewportView(tableTasks);
      if (tableTasks.getColumnModel().getColumnCount() > 0) {
         tableTasks.getColumnModel().getColumn(0).setResizable(false);
         tableTasks.getColumnModel().getColumn(1).setResizable(false);
         tableTasks.getColumnModel().getColumn(1).setPreferredWidth(100);
         tableTasks.getColumnModel().getColumn(2).setResizable(false);
         tableTasks.getColumnModel().getColumn(3).setResizable(false);
         tableTasks.getColumnModel().getColumn(4).setResizable(false);
         tableTasks.getColumnModel().getColumn(5).setResizable(false);
      }

      labelIsInactive.setForeground(new java.awt.Color(255, 0, 51));
      labelIsInactive.setText("Этот аккаунт неактивен");

      jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Задачи пользователя"));

      comboCategories.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            comboCategoriesActionPerformed(evt);
         }
      });

      jLabel2.setText("Категория");

      buttonUpdateTaskList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/retry.png"))); // NOI18N
      buttonUpdateTaskList.setText("<html>    Обновить<br>    <font color=gray size='2'>       Перезагрузить весь список    </font> </html>");
      buttonUpdateTaskList.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonUpdateTaskListActionPerformed(evt);
         }
      });

      jLabel3.setText("Режим просмотра");

      comboViewMode.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "По категориям", "Все сразу", "Только текущие" }));
      comboViewMode.setSelectedItem("Все сразу");
      comboViewMode.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            comboViewModeActionPerformed(evt);
         }
      });

      buttonCreateTask.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/newtask.png"))); // NOI18N
      buttonCreateTask.setText("<html>    Новая задача<br>    <font color=gray size='2'>       Для сотрудника    </font> </html>");
      buttonCreateTask.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
      buttonCreateTask.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
      buttonCreateTask.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonCreateTaskActionPerformed(evt);
         }
      });

      buttonReportTasks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/edit.png"))); // NOI18N
      buttonReportTasks.setText("<html>Детализация задач<br>    <font color=gray size='2'>Отчет по периоду</font> </html>");
      buttonReportTasks.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
      buttonReportTasks.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonReportTasksActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
      jPanel1.setLayout(jPanel1Layout);
      jPanel1Layout.setHorizontalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(buttonUpdateTaskList)
               .addComponent(comboCategories, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addComponent(comboViewMode, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
               .addComponent(buttonCreateTask)
               .addGroup(jPanel1Layout.createSequentialGroup()
                  .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(jLabel2)
                     .addComponent(jLabel3))
                  .addGap(0, 0, Short.MAX_VALUE))
               .addComponent(buttonReportTasks))
            .addContainerGap())
      );
      jPanel1Layout.setVerticalGroup(
         jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jLabel3)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(comboViewMode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jLabel2)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(comboCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(buttonUpdateTaskList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(buttonCreateTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(buttonReportTasks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      );

      labelPhone.setText("Телефон");

      textEmail.setText("e-mail");

      jButton1.setText("Смотреть переписку");
      jButton1.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton1ActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
      getContentPane().setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jLabel1)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                  .addComponent(labelPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addComponent(labelPosition, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addComponent(labelNames, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                     .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                           .addComponent(labelIsInactive, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                           .addComponent(labelPhone)
                           .addComponent(textEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                           .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
               .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 757, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                  .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, Short.MAX_VALUE))
               .addGroup(layout.createSequentialGroup()
                  .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                     .addGroup(layout.createSequentialGroup()
                        .addComponent(labelNames)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelPosition)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelPhone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(labelIsInactive)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                     .addComponent(labelPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                     .addComponent(jLabel1))
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)))
            .addContainerGap())
      );

      pack();
   }// </editor-fold>//GEN-END:initComponents

   private void tableTasksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTasksMouseClicked
      if(evt.getClickCount() == 2) {
         int row = tableTasks.getSelectedRow();

         Task task = new Task();
         task.setId((Long)tableTasks.getModel().getValueAt(row, 0));
         
         ViewTaskFrame viewTaskFrame = new ViewTaskFrame(task, user, SelfProfileInfo.isAdminOrManager());
      }
   }//GEN-LAST:event_tableTasksMouseClicked

   private void comboCategoriesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategoriesActionPerformed
      taskCategory = (TaskCategory) comboCategories.getSelectedItem();
      loadTasks(true, 0);
   }//GEN-LAST:event_comboCategoriesActionPerformed

   private void buttonUpdateTaskListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUpdateTaskListActionPerformed
      loadTasks(true, 0);
      if(viewByCategories)
         updateCategories();
   }//GEN-LAST:event_buttonUpdateTaskListActionPerformed

   private void comboViewModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboViewModeActionPerformed
      viewByCategories = comboViewMode.getSelectedIndex() == 0;
      viewAll = comboViewMode.getSelectedIndex() == 1;
      viewActive = comboViewMode.getSelectedIndex() == 2;
      
      comboCategories.setEnabled(viewByCategories);
      jLabel2.setEnabled(viewByCategories);
      
      loadTasks(true, 0);
      if(viewByCategories)
         updateCategories();
   }//GEN-LAST:event_comboViewModeActionPerformed

   private void buttonCreateTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCreateTaskActionPerformed
      new CreateTaskFrame(user);
   }//GEN-LAST:event_buttonCreateTaskActionPerformed

   private void buttonReportTasksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonReportTasksActionPerformed
      new Thread(()->{
         new DetailedHistoryFrame(user);
      }).start();
   }//GEN-LAST:event_buttonReportTasksActionPerformed

   private void tableTasksMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableTasksMouseReleased
      tableTasksMenu(evt);
   }//GEN-LAST:event_tableTasksMouseReleased

   private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
      open.remove(this.user.getId());
   }//GEN-LAST:event_formWindowClosing

   private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      new DialogFrame(user.getId());
   }//GEN-LAST:event_jButton1ActionPerformed

   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.JButton buttonCreateTask;
   private javax.swing.JButton buttonReportTasks;
   private static javax.swing.JButton buttonUpdateTaskList;
   private javax.swing.JComboBox comboCategories;
   private javax.swing.JComboBox comboViewMode;
   private javax.swing.JButton jButton1;
   private javax.swing.JLabel jLabel1;
   private javax.swing.JLabel jLabel2;
   private javax.swing.JLabel jLabel3;
   private javax.swing.JPanel jPanel1;
   private javax.swing.JScrollPane jScrollPane2;
   private javax.swing.JLabel labelIsInactive;
   private javax.swing.JLabel labelNames;
   private javax.swing.JLabel labelPhone;
   private javax.swing.JLabel labelPhoto;
   private javax.swing.JLabel labelPosition;
   private static javax.swing.JTable tableTasks;
   private javax.swing.JTextField textEmail;
   // End of variables declaration//GEN-END:variables
}
