/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import org.getmansky.gui.frame.tasks.CreateTaskFrame;
import org.getmansky.gui.frame.account.NewUserFrame;
import org.getmansky.gui.frame.account.SelfProfileInfo;
import org.getmansky.helper.Helper;
import org.getmansky.gui.frame.account.ViewUserFrame;
import org.getmansky.gui.frame.messages.SendBroadcastFrame;
import org.getmansky.net.Network;
import org.getmansky.net.json.Group;
import org.getmansky.net.json.User;

/**
 *
 * @author OlegusGetman
 */
public class AdminPane extends javax.swing.JPanel {

   private boolean viewInactive;

   /**
    * Creates new form AdminPane
    */
   public AdminPane() {
      initComponents();

      new Thread(() -> {
         try {
            Network
                    .groups()
                    .getGroups()
                    .stream()
                    .forEach((group) -> {
                       comboEditGroup.addItem(group);
                    });
         } catch (IOException e) {
            Helper.showMessage("Ошибка сетевого запроса");
         }
      }).start();
      
      tableUsers.setAutoCreateRowSorter(true);
      tableUsers.getTableHeader().setReorderingAllowed(false);
      tableUsers.removeColumn(tableUsers.getColumnModel().getColumn(0));
      tableUsers.removeColumn(tableUsers.getColumnModel().getColumn(0));
      tableUsers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      tableUsers.getSelectionModel().addListSelectionListener((ListSelectionEvent event) -> {
         if (viewInactive) {
            return;
         }
         int selRow = tableUsers.getSelectedRow();
         selRow = tableUsers.convertRowIndexToModel(selRow);
         if (selRow > -1) {
            try {
               textEditFname.setText(tableUsers.getModel().getValueAt(selRow, 2).toString());
               textEditName.setText(tableUsers.getModel().getValueAt(selRow, 3).toString());
               textEditOname.setText(tableUsers.getModel().getValueAt(selRow, 4).toString());
               textEditPosition.setText(tableUsers.getModel().getValueAt(selRow, 5).toString());
               comboEditGroup.setSelectedItem(Network.groups().findById(
                       (int) tableUsers.getModel().getValueAt(selRow, 1)
               ));
               comboEditGroup.setEnabled(!viewInactive && SelfProfileInfo.isAdmin() 
                  && SelfProfileInfo.id != (long)tableUsers.getModel().getValueAt(selRow, 0));
               long userId = (long) tableUsers.getModel().getValueAt(selRow, 0);
               buttonDeleteUser.setEnabled(!viewInactive && SelfProfileInfo.isAdmin() && SelfProfileInfo.id != userId);
            } catch (IOException ex) {
               Helper.showMessage("Ошибка сетевого запроса");
            }
         } else {
            textEditFname.setText("");
            textEditName.setText("");
            textEditOname.setText("");
            textEditPosition.setText("");
         }
      });
      
      comboEditGroup.setEnabled(SelfProfileInfo.isAdmin());
      buttonRegisterUser.setEnabled(SelfProfileInfo.isAdmin());
      buttonDeleteUser.setEnabled(SelfProfileInfo.isAdmin());
      
      setupKeyboardShortcuts();
      loadUsersList(true, 0);
   }
   
   private void setupKeyboardShortcuts() {
      InputMap im = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
      ActionMap am = getActionMap();
      
      im.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "refresh");
      am.put("refresh", new AbstractAction() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if(buttonUpdateUsersList.isEnabled())
               loadUsersList(true, 0);
         }
      });
   }

   void setSorting(JTable jTable) {
      try {
         /*RowSorter<TableModel> sorter = new TableRowSorter(jTable.getModel());
          List<RowSorter.SortKey> sortKeys = new ArrayList();
          sortKeys.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
          sorter.setSortKeys(sortKeys); 
          jTable.setRowSorter(sorter);*/
      } catch (NullPointerException e) {

      }
   }

   final void loadUsersList(boolean clean, long offset) {
      buttonUpdateUsersList.setEnabled(false);
      new Thread(() -> {
         setSorting(tableUsers);
         DefaultTableModel model = (DefaultTableModel) tableUsers.getModel();
         if (clean) {
            model.setRowCount(0);
         }
         try {
            User[] users;
            String pre;
            String post;
            if (!viewInactive) {
               users = Network.users().getUsers();
               pre = "";
               post = "";
            } else {
               users = Network.inactiveUsers(offset);
               pre = "<html><font color=red>";
               post = "</font></html>";
            }
            Helper.unsortTable(tableUsers);
            for (User user : users) {
               model.addRow(new Object[]{
                  user.getId(),
                  user.getGroupId(),
                  pre + user.getFname() + post,
                  pre + user.getName() + post,
                  pre + user.getOname() + post,
                  pre + user.getPosition() + post,
                  pre + user.getLogin() + post,
                  pre + user.getEmail() + post,
                  pre + Network.groups().findById(user.getGroupId()).getTitle() + post
               });
            }
         } catch (IOException e) {
            Helper.showMessage("Ошибка сетевого запроса");
         } finally {
            buttonUpdateUsersList.setEnabled(true);
         }
      }).start();
   }

   void updateSelectedUser() {
      buttonUpdateUserInfo.setEnabled(false);
      new Thread(() -> {
         int selRow = tableUsers.getSelectedRow();
         selRow = tableUsers.convertRowIndexToModel(selRow);
         if(selRow == -1) {
            buttonUpdateUserInfo.setEnabled(true);
            return;
         }
         User user = new User();
         user.setId((long) tableUsers.getModel().getValueAt(selRow, 0));
         user.setName(textEditName.getText());
         user.setFname(textEditFname.getText());
         user.setOname(textEditOname.getText());
         user.setPosition(textEditPosition.getText());
         user.setGroupId(((Group) comboEditGroup.getSelectedItem()).getId());
         
         try {
            Network.editUser(user);
         } catch (IOException ex) {
            Logger.getLogger(AdminPane.class.getName()).log(Level.SEVERE, null, ex);
         } finally {
            loadUsersList(true, 0);
            buttonUpdateUserInfo.setEnabled(true);
         }
      }).start();
   }

   void deleteSelectedUser() {
      buttonDeleteUser.setEnabled(false);
      new Thread(() -> {
         int selRow = tableUsers.getSelectedRow();
         selRow = tableUsers.convertRowIndexToModel(selRow);

         User user = new User();
         user.setId((long) tableUsers.getModel().getValueAt(selRow, 0));

         try {
            Network.deleteUser(user);
         } catch (IOException ex) {
            Logger.getLogger(AdminPane.class.getName()).log(Level.SEVERE, null, ex);
         } finally {
            loadUsersList(true, 0);
            buttonDeleteUser.setEnabled(true);
         }
      }).start();
   }

   /**
    * This method is called from within the constructor to initialize the form.
    * WARNING: Do NOT modify this code. The content of this method is always
    * regenerated by the Form Editor.
    */
   @SuppressWarnings("unchecked")
   // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
   private void initComponents() {

      jPanel2 = new javax.swing.JPanel();
      jScrollPane2 = new javax.swing.JScrollPane();
      tableUsers = new javax.swing.JTable();
      buttonUpdateUsersList = new javax.swing.JButton();
      textEditFname = new javax.swing.JTextField();
      textEditName = new javax.swing.JTextField();
      textEditOname = new javax.swing.JTextField();
      textEditPosition = new javax.swing.JTextField();
      buttonUpdateUserInfo = new javax.swing.JButton();
      comboEditGroup = new javax.swing.JComboBox();
      buttonDeleteUser = new javax.swing.JButton();
      buttonCreateTask = new javax.swing.JButton();
      buttonRegisterUser = new javax.swing.JButton();
      buttonLoadMore = new javax.swing.JButton();
      jLabel1 = new javax.swing.JLabel();
      comboView = new javax.swing.JComboBox();
      buttonActivate = new javax.swing.JButton();
      buttonCreateBroadcast = new javax.swing.JButton();

      jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Управление аккаунтами"));

      tableUsers.setModel(new javax.swing.table.DefaultTableModel(
         new Object [][] {

         },
         new String [] {
            "id", "groupId", "Фамилия", "Имя", "Отчество", "Должность", "Логин", "E-mail", "Группа"
         }
      ) {
         Class[] types = new Class [] {
            java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
         };
         boolean[] canEdit = new boolean [] {
            false, false, false, false, false, false, false, false, false
         };

         public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
         }

         public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
         }
      });
      tableUsers.setToolTipText("");
      tableUsers.addMouseListener(new java.awt.event.MouseAdapter() {
         public void mouseClicked(java.awt.event.MouseEvent evt) {
            tableUsersMouseClicked(evt);
         }
         public void mouseReleased(java.awt.event.MouseEvent evt) {
            tableUsersMouseReleased(evt);
         }
      });
      jScrollPane2.setViewportView(tableUsers);

      buttonUpdateUsersList.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/retry.png"))); // NOI18N
      buttonUpdateUsersList.setText("<html>    Обновить<br>    <font color=gray size='2'>       Перезагрузить весь список    </font> </html>");
      buttonUpdateUsersList.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonUpdateUsersListActionPerformed(evt);
         }
      });

      buttonUpdateUserInfo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/edit.png"))); // NOI18N
      buttonUpdateUserInfo.setText("<html>    Редактировать<br>    <font color=gray size='2'>       Применить изменения    </font> </html>");
      buttonUpdateUserInfo.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonUpdateUserInfoActionPerformed(evt);
         }
      });

      comboEditGroup.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            comboEditGroupActionPerformed(evt);
         }
      });

      buttonDeleteUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/delete.png"))); // NOI18N
      buttonDeleteUser.setText("<html>    Деактивировать<br>    <font color=gray size='2'>       Удалить аккаунт    </font> </html>");
      buttonDeleteUser.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonDeleteUserActionPerformed(evt);
         }
      });

      buttonCreateTask.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/newtask.png"))); // NOI18N
      buttonCreateTask.setText("<html>    Новая задача<br>    <font color=gray size='2'>       Для сотрудника    </font> </html>");
      buttonCreateTask.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonCreateTaskActionPerformed(evt);
         }
      });

      buttonRegisterUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/add.png"))); // NOI18N
      buttonRegisterUser.setText("<html>    Регистрация<br>    <font color=gray size='2'>       Добавить аккаунт    </font> </html>");
      buttonRegisterUser.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonRegisterUserActionPerformed(evt);
         }
      });

      buttonLoadMore.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/more.png"))); // NOI18N
      buttonLoadMore.setText("<html>    Загрузить еще<br>    <font color=gray size='2'>       Следующие 50 записей    </font> </html>");
      buttonLoadMore.setEnabled(false);
      buttonLoadMore.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonLoadMoreActionPerformed(evt);
         }
      });

      jLabel1.setText("Просмотр");

      comboView.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Активные аккаунты", "Удаленные аккаунты" }));
      comboView.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            comboViewActionPerformed(evt);
         }
      });

      buttonActivate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/activate.png"))); // NOI18N
      buttonActivate.setText("<html>    Активировать<br>    <font color=gray size='2'>       Восстановить аккаунт    </font> </html>");
      buttonActivate.setEnabled(false);
      buttonActivate.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonActivateActionPerformed(evt);
         }
      });

      buttonCreateBroadcast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/getmansky/gui/graphics/buttons/email.png"))); // NOI18N
      buttonCreateBroadcast.setText("<html>    Создать рассылку<br>    <font color=gray size='2'>       Сообщение сотрудникам    </font> </html> ");
      buttonCreateBroadcast.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(java.awt.event.ActionEvent evt) {
            buttonCreateBroadcastActionPerformed(evt);
         }
      });

      javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
      jPanel2.setLayout(jPanel2Layout);
      jPanel2Layout.setHorizontalGroup(
         jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addComponent(jScrollPane2)
               .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                  .addComponent(textEditFname, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(textEditName, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(textEditOname, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(textEditPosition)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(comboEditGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addGroup(jPanel2Layout.createSequentialGroup()
                  .addComponent(buttonCreateTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonUpdateUserInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonRegisterUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonDeleteUser, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonActivate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
               .addGroup(jPanel2Layout.createSequentialGroup()
                  .addComponent(buttonUpdateUsersList, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonLoadMore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(buttonCreateBroadcast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(jLabel1)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(comboView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
      );
      jPanel2Layout.setVerticalGroup(
         jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(buttonUpdateUsersList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonLoadMore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(jLabel1)
               .addComponent(comboView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonCreateBroadcast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(textEditFname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(textEditName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(textEditOname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(textEditPosition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(comboEditGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
               .addComponent(buttonUpdateUserInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonCreateTask, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonRegisterUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonDeleteUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
               .addComponent(buttonActivate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(0, 0, 0))
      );

      javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
      this.setLayout(layout);
      layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGap(96, 96, 96))
      );
      layout.setVerticalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
         .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap())
      );
   }// </editor-fold>//GEN-END:initComponents

    private void buttonUpdateUsersListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUpdateUsersListActionPerformed
       loadUsersList(true, 0);
    }//GEN-LAST:event_buttonUpdateUsersListActionPerformed

    private void buttonUpdateUserInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUpdateUserInfoActionPerformed
       updateSelectedUser();
    }//GEN-LAST:event_buttonUpdateUserInfoActionPerformed

    private void buttonDeleteUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteUserActionPerformed
       if (Helper.ask("Деактивировать аккаунт?")) {
          deleteSelectedUser();
       }
    }//GEN-LAST:event_buttonDeleteUserActionPerformed

   private void buttonCreateTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCreateTaskActionPerformed
      int selRow = tableUsers.getSelectedRow();
      if (selRow == -1) {
         return;
      }

      User user = new User();
      user.setId((long) tableUsers.getModel().getValueAt(selRow, 0));

      CreateTaskFrame createTaskFrame = new CreateTaskFrame(user);
   }//GEN-LAST:event_buttonCreateTaskActionPerformed

   private void tableUsersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableUsersMouseClicked
      if (evt.getClickCount() == 2) {
         int selRow = tableUsers.getSelectedRow();
         selRow = tableUsers.convertRowIndexToModel(selRow);
         if (selRow == -1) {
            return;
         }
         long userId = (long) tableUsers.getModel().getValueAt(selRow, 0);
         new ViewUserFrame(userId);
      }
   }//GEN-LAST:event_tableUsersMouseClicked

   private void buttonRegisterUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRegisterUserActionPerformed
      new NewUserFrame();
   }//GEN-LAST:event_buttonRegisterUserActionPerformed

   private void comboViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboViewActionPerformed
      viewInactive = comboView.getSelectedIndex() == 1;

      buttonCreateTask.setEnabled(!viewInactive);
      buttonUpdateUserInfo.setEnabled(!viewInactive || !SelfProfileInfo.isAdmin());
      buttonDeleteUser.setEnabled(!viewInactive && SelfProfileInfo.isAdmin());
      buttonLoadMore.setEnabled(viewInactive);
      textEditFname.setEnabled(!viewInactive);
      textEditName.setEnabled(!viewInactive);
      textEditOname.setEnabled(!viewInactive);
      textEditPosition.setEnabled(!viewInactive);
      buttonActivate.setEnabled(viewInactive && SelfProfileInfo.isAdmin());
      
      loadUsersList(true, 0);
   }//GEN-LAST:event_comboViewActionPerformed

   private void buttonLoadMoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLoadMoreActionPerformed
      loadUsersList(false, tableUsers.getRowCount());
   }//GEN-LAST:event_buttonLoadMoreActionPerformed

   private void buttonActivateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonActivateActionPerformed
      buttonActivate.setEnabled(false);
      new Thread(() -> {
         int selRow = tableUsers.getSelectedRow();
         selRow = tableUsers.convertRowIndexToModel(selRow);

         User user = new User();
         user.setId((long) tableUsers.getModel().getValueAt(selRow, 0));

         try {
            Network.activateUser(user.getId());
            Helper.showMessage("Аккаунт восстановлен");
         } catch (IOException ex) {
            Helper.showMessage("Ошибка запроса: " + ex.getMessage());
         } finally {
            loadUsersList(true, 0);
            buttonActivate.setEnabled(true);
         }
      }).start();
   }//GEN-LAST:event_buttonActivateActionPerformed

   private void buttonCreateBroadcastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCreateBroadcastActionPerformed
      new SendBroadcastFrame();
   }//GEN-LAST:event_buttonCreateBroadcastActionPerformed

   private void comboEditGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboEditGroupActionPerformed
      // TODO add your handling code here:
   }//GEN-LAST:event_comboEditGroupActionPerformed

   private void tableUsersMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableUsersMouseReleased
      JMenuItem miProfile = new JMenuItem("Профиль");
      miProfile.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            int selRow = tableUsers.getSelectedRow();
            selRow = tableUsers.convertRowIndexToModel(selRow);
            if (selRow == -1) {
               return;
            }
            long userId = (long) tableUsers.getModel().getValueAt(selRow, 0);
            new ViewUserFrame(userId);
         }
      });
      JPopupMenu popup = new JPopupMenu();
      popup.add(miProfile);
      
      Helper.showTableContextMenu(evt, tableUsers, popup);
   }//GEN-LAST:event_tableUsersMouseReleased


   // Variables declaration - do not modify//GEN-BEGIN:variables
   private javax.swing.JButton buttonActivate;
   private javax.swing.JButton buttonCreateBroadcast;
   private javax.swing.JButton buttonCreateTask;
   private javax.swing.JButton buttonDeleteUser;
   private javax.swing.JButton buttonLoadMore;
   private javax.swing.JButton buttonRegisterUser;
   private javax.swing.JButton buttonUpdateUserInfo;
   private javax.swing.JButton buttonUpdateUsersList;
   private javax.swing.JComboBox comboEditGroup;
   private javax.swing.JComboBox comboView;
   private javax.swing.JLabel jLabel1;
   private javax.swing.JPanel jPanel2;
   private javax.swing.JScrollPane jScrollPane2;
   private javax.swing.JTable tableUsers;
   private javax.swing.JTextField textEditFname;
   private javax.swing.JTextField textEditName;
   private javax.swing.JTextField textEditOname;
   private javax.swing.JTextField textEditPosition;
   // End of variables declaration//GEN-END:variables
}
