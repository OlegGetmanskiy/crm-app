/*
This file is part of Light Objective.

Light Objective is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Light Objective is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Light Objective. If not, see <http://www.gnu.org/licenses/>.

Copyright Oleg Getmansky 2015
*/
package org.getmansky;

import java.util.prefs.Preferences;
import org.getmansky.helper.Helper;

/**
 *
 * @author OlegusGetman
 */
public class Settings {
   public final static int NOTIFY_NONE = 2;
   public final static int NOTIFY_TRAY = 1;
   public final static int NOTIFY_TRAY_SOUND = 0;
   
   public final static int CLOSE_DISPOSE = 1;
   public final static int CLOSE_TRAY = 0;
   
   public static String storedLogin;
   public static String storedPassword;
   
   public static int notificationBehavior = NOTIFY_TRAY_SOUND;
   public static int closeBehavior = CLOSE_TRAY;
   public static boolean trayMessageShown = false;
   public static String serverAddress;
   public static String savePath = System.getProperty("user.dir");
   
   private static Preferences p = Preferences.userRoot();
   
   public static void apply() {
      p.putInt("notificationBehavior", notificationBehavior);
      p.putInt("closeBehavior", closeBehavior);
      p.putBoolean("trayMessageShown", trayMessageShown);
      p.put("serverAddress", serverAddress);
      p.put("savePath", savePath);
   }
   
   public static void restore() {
      try {
         notificationBehavior = p.getInt("notificationBehavior", NOTIFY_TRAY_SOUND);
         closeBehavior = p.getInt("closeBehavior", CLOSE_TRAY);
         trayMessageShown = p.getBoolean("trayMessageShown", false);
         serverAddress = p.get("serverAddress", "http://olegusgetman.ru:3001/");
         savePath = p.get("savePath", savePath);
      } catch(Exception e) {
         Helper.showMessage("Ошибка при чтении настроек");
         e.printStackTrace();
      }
   }
   
   public static void storeCredentials() {
      p.put("storedLogin", storedLogin);
      p.put("storedPassword", storedPassword);
   }
   
   public static void restoreCredentials() {
      try {
         storedLogin = p.get("storedLogin", "");
         storedPassword = p.get("storedPassword", "");
      } catch(Exception e) {
         Helper.showMessage("Ошибка при чтении настроек");
         e.printStackTrace();
      }
   }
}
